/* Public function prototypes -----------------------------------------------*/
inline void init_Timer();
inline void init_SysTick();
inline void init_Crystal();
inline void init_EXTI0();
inline void init_GPIO();

void TIM2_IRQHandler();

