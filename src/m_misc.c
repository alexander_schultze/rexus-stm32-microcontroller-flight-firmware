#include "includes.h"



/* Helper Functions */
/* http://www.eagleairaust.com.au/code/crc16.htm */
u16 crc16_ccitt(u16 crc, u8 ser_data)
{
	crc  = (u8)(crc >> 8) | (crc << 8);
	crc ^= ser_data;
	crc ^= (u8)(crc & 0xff) >> 4;
	crc ^= (crc << 8) << 4;
	crc ^= ((crc & 0xff) << 4) << 1;

	return crc;
}
u16 crc16(const u8 *p, int len)
{
	int i;
	u16 crc = 0xFFFF;

	for (i=0; i<len; i++)
		crc = crc16_ccitt(crc, p[i]);

	return crc;
}






/* Task Manager Variables
		 *
		 * Captured Tasks
		 * [0] - IDLE
		 * [1] - Scheduler. Retrieve Values.
		 * [2] - Scheduler. Packing&Sending Values.
		 * [3] - Scheduler. Write SD Card
		 * [4] - [async]Control.
		 * [5] -
		 * [6] - [async]Interrupt. Handler SPI
		 */
		u8 			  _scheduler_task=0;
		unsigned int  _scheduler_tasktimes[TASKS_COUNT];
		unsigned int  _scheduler_tasktimes_1s[TASKS_COUNT];   /* Task Time Current in 0.01 ms         */
		unsigned int  _scheduler_tasktimes_10s[TASKS_COUNT];   /* Task time combined in ms         */


	

inline void fipex_scheduler_perfmon_starttask(u8 task_nr){
/* Supply information about task start times to evaluate the time needed */
/* ticks is increased automatically by timer every 0.01ms                 */

/* First store the accumulated new ticks to all the registers */
#ifdef useTaskMonitor


/* Add little helpers to return to an original task_nr by parameter 255 */

static u8 last_task=0;
last_task=_scheduler_task;




if(task_nr==1){
	u8 i;
	for(i=1;i<TASKS_COUNT;i++)_scheduler_tasktimes[i]=0;
}

_scheduler_tasktimes[_scheduler_task]=ticks;
_scheduler_tasktimes_1s[_scheduler_task]=_scheduler_tasktimes_1s[_scheduler_task]+ticks;
_scheduler_tasktimes_10s[_scheduler_task]=_scheduler_tasktimes_10s[_scheduler_task]+ticks;
/* Start the New Task or Retourn to Original*/
if(task_nr==255){_scheduler_task=last_task;}else{_scheduler_task=task_nr;}
ticks=0;
#endif

}






unsigned char msg_buffer[30]; /* Used by several function when creating outgoing packages */

inline void clear_buffer()
                    {
                	int i=0;
                      for(i=0; i<(sizeof(msg_buffer)-1);i++);
                      msg_buffer[i]='\0';
                    }
/* A simplified pow function for unsigned integers, avoiding the heavy libraries */
inline unsigned int __pow(unsigned int base, unsigned int exp){
	if((base==2)&&(exp==8)) return 256;
	if((base==2)&&(exp==16)) return 65536;
	if((exp==0)) return 1;

	int i;
	int value=1;
	for(i=1;i<=exp;i++){
		value*=base;
	}
	return value;
}



inline uint16_t double_to_u16(double parDouble){
	return (uint16_t) (parDouble*1000);

}


/* Set Error, Status, Hatch Flags */

inline void setFlagValue(uint8_t* parVariables, uint8_t parFlag, uint8_t parValue){
	if(parValue==0){
*parVariables &= ~parFlag;

		} else {
		*parVariables |=parFlag;
		}
	 

}

inline void setFlag(uint8_t* parVariables, uint8_t parFlag){
	 *parVariables |=parFlag;

}

inline void clearFlag(uint8_t* parVariables, uint8_t parFlag){
	 *parVariables &= ~parFlag;

}

inline uint8_t getFlag(uint8_t* parVariables, uint8_t parFlag){

	return ((*parVariables & parFlag)>0);
}


#define STM32_DELAY_US_MULT         12

void Delay_us(unsigned int us)
{
    us *= STM32_DELAY_US_MULT;

    /* fudge for function call overhead  */
    //us--;
    asm volatile("   mov r0, %[us]          \n\t"
                 "1: subs r0, #1            \n\t"
                 "   bhi 1b                 \n\t"
                 :
                 : [us] "r"(us)
                 : "r0");
}

void Delay_ms(unsigned int ms)
{
    Delay_us(1000 * ms);
}

