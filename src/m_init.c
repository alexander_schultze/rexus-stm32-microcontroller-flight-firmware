﻿#include "includes.h"
/* Private variables ---------------------------------------------------------*/



inline void init_Timer(){
	NVIC_InitTypeDef   NVIC_InitStructure;
	  /*Time base configuration
	   *
	   * ABP1 - 72 MHz
	   */


	/* --------- Timer 4 ------ [0.1 second] ------------ */

	  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	  NVIC_Init(&NVIC_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	// Time Base initialisieren
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Prescaler = 10000-1; // 	72MHz / 10000 = 7.2 kHz nach Prescaler
	TIM_TimeBaseInitStructure.TIM_Period = 720 - 1;  //	7.2khz / 720 = 10 Hz endg�ltige Updaterate des Timers
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStructure);

	  // External Trigger set to External Clock, Mode 1
	//  TIM_ETRClockMode2Config (TIM4, TIM_ExtTRGPSC_OFF, TIM_ExtTRGPolarity_NonInverted, 0);

	  // Input Trigger selection
	//  TIM_TIxExternalClockConfig (TIM4, TIM_TIxExternalCLK1Source_TI1, TIM_ICPolarity_Rising, 0) ;
		// Interrupt enable f�r Update

	/* Interrupt nicht aktiviert fuer debugging*/
	TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);

		// um den Timer loslaufen zu lassen
		TIM_Cmd(TIM4,ENABLE);




#ifdef useTaskMonitor
			/* --------- Timer 3 ------ [0.01 ms] ------------ */
		  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
		  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
		  /* Zeitkritisch daher hohe Priorit�t */
		  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;
		  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		  NVIC_Init(&NVIC_InitStructure);
			// Time Base initialisieren
			TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
			TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
			TIM_TimeBaseInitStructure.TIM_Prescaler = 10-1; // 	72MHz / 10 = 7200 kHz nach Prescaler
			TIM_TimeBaseInitStructure.TIM_Period = 72 - 1;  //	7200 khz / 72 = 1'000 Hz endg�ltige Updaterate des Timers
			TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
			TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);

			TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
			TIM_Cmd(TIM3,ENABLE);
#endif

			/* --------- Timer 2 ------ [10 ms] ------------ */
		  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
		  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
		  /* Zeitkritisch daher hohe Priorit�t */
		  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
		  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		  NVIC_Init(&NVIC_InitStructure);
			// Time Base initialisieren
			TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
			TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
			TIM_TimeBaseInitStructure.TIM_Prescaler = 720-1; // 	72MHz / 720 = 100 kHz nach Prescaler
			TIM_TimeBaseInitStructure.TIM_Period = 1000 - 1;  //	7200 khz / 720 = 50 Hz endg�ltige Updaterate des Timers
			TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
			TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

			TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
			TIM_Cmd(TIM2,ENABLE);





return;
}









inline void  init_SysTick(){
  /* Setup SysTick Timer for 10 msec interrupts  */
  if (SysTick_Config((SystemCoreClock)/100)) {
	  // Original CoreClock/100

    /* Capture error */
	  printf(".fault.:Systick_Config\n\r");
  }
  NVIC_SetPriority(SysTick_IRQn, 0x0F);
  /* Niedrige Priorit�t */
  return;
}


inline  void init_Crystal(){




	  /* Enable PWR and BKP clocks */
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

	  /* Allow access to BKP Domain */
	  PWR_BackupAccessCmd(ENABLE);

	  /* Reset Backup Domain */
	//  BKP_DeInit(); Is not necessary so can be switched off.

	  /* Enable LSE */
	  RCC_LSEConfig(RCC_LSE_ON);

	  /* Wait till LSE is ready */
	  while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);

	//RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE)
  }




 inline void init_Watchdog(){
	/* The watchdog is supposed to check for mistakes in the main program routine if it is possible to hang.
	 * It is fed by internal 40KHz clock. This settings counts down about 2 seconds before reset.
	 * I suggest avoiding using the IWDG during debugging and include it in the final compilation.

	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);

	IWDG_SetPrescaler(IWDG_Prescaler_32);

	IWDG_SetReload(4095);

	IWDG_ReloadCounter();

	IWDG_Enable(); */

}



/**
  * @brief  This function handles Tim4 (1second) interrupt request.
  * @signal Liftoff
  * @param  None
  * @retval None
  */

void TIM4_IRQHandler(){
while( (TIM4->SR & (1<<0)) > 0 ) TIM4->SR &= ~(1<<0);

fipex_time_sys++;
if(current_state>=FLIGHT)fipex_time_lo++;

timeline_run();

#ifdef useTaskMonitor

u8 i;
if (fipex_time_sys%10==0){
	for(i=0;i<TASKS_COUNT;i++)
		{
		_scheduler_tasktimes_1s[i]=0;
		_scheduler_tasktimes_10s[i]=0;
		}

}else{
	for(i=0;i<TASKS_COUNT;i++)
		{
		_scheduler_tasktimes_1s[i]=0;
		}

}


#endif



}
/**
  * @brief  This function handles Tim3 (0.01ms) interrupt request. It is ONLY used for TaskMonitor and can be disabled to improve performance.
  * @signal None
  * @param  None
  * @retval None
  */
void TIM3_IRQHandler(){
while( (TIM3->SR & (1<<0)) > 0 ) TIM3->SR &= ~(1<<0);
fipex_ticks++;
}

/**
  * @brief  This function handles Tim2 (20ms) interrupt request. It is ONLY used for time-critical FipexControl.
  * @signal None
  * @param  None
  * @retval None
  */

void TIM2_IRQHandler(){
while( (TIM2->SR & (1<<0)) > 0 ) TIM2->SR &= ~(1<<0);


//main_led_soe_on();
fipex_control();
//main_led_soe_off();

}







