
enum eGain{
	GAIN_OFF=0,
	GAIN1,
	GAIN10,
	GAIN100,
	GAIN1000
	/* Gain | ADG  	|A0(=PGA1)|A1(=PGA2)
	 * 1	| 4		| 1		  | 1
	 * 10	| 3		| 1		  | 0
	 * 100	| 2		| 0		  | 1
	 * 1000 | 1		| 0		  | 0
	 */
};

enum eDevice{
	SENSOR1=0,
	SENSOR2,
	SENSOR3,
	HEATING1,
	HEATING2,
	HEATING3,
};

struct sDevice {
	/* ADC Direct Measured Values. Ref Voltage 3.3V */
	uint8_t Device;
	volatile uint16_t ADC_U;
	volatile uint16_t ADC_I;

	volatile uint8_t Gain;

	/* Hardware Definitions */
	GPIO_TypeDef* Port_GainSelect;
	uint16_t Pin_Gain1;
	uint16_t Pin_Gain2;

	uint8_t DAC_Channel;

	/* Software Definitions */
	uint8_t ADC_Channel_U;
	uint8_t ADC_Channel_I;

	/* PID Control          */
	struct sPID PID;
	volatile double targetValue;
	volatile double processValue;
	volatile uint16_t setValue;
	/* Results          */



} _sDevices[6];


/* Private Prototypes */

//double fipex_pid (struct sPID* argPID, double sp, double pv);
double fipex_PT_calcResistance(u16 parTemp, double parRo);
inline u16 fipex_control_heating(u8 i);
inline u16 fipex_control_sensor(u8 i);
void fipex_control_init();
void fipex_setGain(u8 i);

/* Public Prototypes */
extern inline void fipex_control(void);
/* Structs */

