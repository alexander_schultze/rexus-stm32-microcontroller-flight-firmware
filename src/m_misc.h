#define TASKS_COUNT 7

unsigned char msg_buffer[30];

u16 crc16_ccitt(u16 crc, u8 ser_data);



inline void fipex_scheduler_perfmon_starttask(u8 task_nr);
unsigned int  _scheduler_tasktimes[TASKS_COUNT];
unsigned int  _scheduler_tasktimes_1s[TASKS_COUNT];   /* Task Time Current in 0.01 ms         */
unsigned int  _scheduler_tasktimes_10s[TASKS_COUNT];   /* Task time combined in ms         */

inline void clear_buffer();
inline unsigned int __pow(unsigned int base, unsigned int exp);
inline uint16_t double_to_u16(double parDouble);

/* Flag Masking */
inline void setFlagValue(uint8_t* parVariables, uint8_t parFlag, uint8_t parValue);

inline void setFlag(uint8_t* parVariables, uint8_t parFlag);

inline void clearFlag(uint8_t* parVariables, uint8_t parFlag);

inline uint8_t getFlag(uint8_t* parVariables, uint8_t parFlag);

#define STM32_DELAY_US_MULT         12

void Delay_us(unsigned int us);

void Delay_ms(unsigned int ms);