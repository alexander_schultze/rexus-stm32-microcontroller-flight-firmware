/* Public Variables Declaration */




volatile uint32_t fipex_ticks;
volatile uint32_t fipex_time_sys;
volatile uint32_t fipex_time_lo;

/* Binary Coded Flags */
uint8_t fipex_HatchFlags;
uint8_t fipex_StatusFlags;
uint8_t fipex_ErrorFlags;



uint16_t fipex_adc_values[14];
uint16_t fipex_adc2_values[5];
int16_t  fipex_temp_values[6];

uint8_t fipex_status_hatch_counter;

/* Public Functions Prototypes */

void fipex_usart_handler(uint8_t *data);

void fipex_init();
void fipex_init_sdcard();
void write_status_lcd();
void printdebug();
void fipex_i2c_timerproc();
void fipex_scheduler();
/* Temp: Private Functions Prototypes */
void _fipex_retrieve_ADC_values();
void _fipex_sendPackage_Infostring_Add(char parChar);
/*Public Variables Declaration */
