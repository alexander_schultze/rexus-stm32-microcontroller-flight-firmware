﻿/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		15.02.2013
 *
 *
 * -------------------------------------------------------------
 *			File Hierachy
 * -------------------------------------------------------------
 *			STM32 related: /syscalls /stmlib /semihosting /cmsis_boot /cmsis
 *			HAL			 : /HAL LIBS
 *							SD Card: ff, diskio,ffconf
 *							LCD	   : lcd3310
 *							ADC(STM):stm_32adc
 *			Project Files: Interrupt Handler: m_interrupts (tbr)
 *			               Init Routines    : m_init (tbr)
 *
 *
 *
 *  -------------------------------------------------------------
 *          Information Section
 *  -------------------------------------------------------------
 *  One word on Compiling Options. Best Debugging possible with Optimizate off (-o0)
 *  F103RBT6 has 128 kbyte flash in 128 pages.
 *  For Flash Memory Saving, Reserve Page 127, 126 for CONSTANT DATA
 *  0x0800 0000 - 0x0800 03FF (p1)
 *  0x0801 F800- 0x0801 FBFF (p126)
 *  0x0801 FC00 - 0x0801 FFFF (p127)
 *  -------------------------------------------------------------
 *          USART Settings
 *  -------------------------------------------------------------
 *	    Baud: 38k4, 8 Bits, 1 Stopbit, No Parity
 * -------------------------------------------------------------
 *          Comments Section
 *  -------------------------------------------------------------
 *  PA4/5 is USART2 ? Coincendes with RS422 port! Should be changed in final design!!
 *  We wanted to use PA4/5 for DAC, but thats not onboard anyways ..
 *
 *
 */

#include "includes.h"


/* Private Variables -----------------------------------------------------------*/

uint32_t program_runs;





volatile EXP_STATE current_state;

volatile uint8_t scheduler_100ms_request=FALSE;
uint8_t scheduler_100ms_request_timeout=0;
volatile uint8_t scheduler_10ms_request=FALSE;
uint8_t scheduler_10ms_request_timeout=0;

/* Private Functions Prototypes */

int main_getRunsfromFlash();
void main_hatch_trigger_proc();
void main_showLedStatus();

#ifdef useFatFS
FATFS fs[1];
FIL file_log, file_measurements;
#endif


/* Hardware Helper Functions */
/* P103 LED C12, Moxa B9 .  */


void inline main_led_status_on(){ GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_SET);}
void inline main_led_status_off(){ GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_RESET);}


void inline main_led_soe_on(){ GPIO_WriteBit(GPIOB, GPIO_Pin_9, Bit_SET);}
void inline main_led_soe_off(){ GPIO_WriteBit(GPIOB, GPIO_Pin_9, Bit_RESET);}

void inline main_hatch_on(){
	GPIO_WriteBit(GPIOA, GPIO_Pin_10, Bit_SET);
	setFlagValue(&fipex_HatchFlags,h_o_hatchOn,1);
}

void inline main_hatch_off(){
	GPIO_WriteBit(GPIOA, GPIO_Pin_10, Bit_RESET);
	setFlagValue(&fipex_HatchFlags,h_o_hatchOn,0);
}

void inline main_hatch_charge_on(){
	GPIO_WriteBit(GPIOB, GPIO_Pin_2, Bit_SET);
	setFlagValue(&fipex_HatchFlags,h_o_chargeOn,1);}

void inline main_hatch_charge_off(){
	GPIO_WriteBit(GPIOB, GPIO_Pin_2, Bit_RESET);
	setFlagValue(&fipex_HatchFlags,h_o_chargeOn,0);
}

void inline main_bat_on(){
	GPIO_WriteBit(GPIOC, GPIO_Pin_13, Bit_SET);
	setFlagValue(&fipex_StatusFlags,s_o_batteryOn,1);}
void inline main_bat_off(){
	GPIO_WriteBit(GPIOC, GPIO_Pin_13, Bit_RESET);
	setFlagValue(&fipex_StatusFlags,s_o_batteryOn,0);}

u8 inline main_hatch_in1(){return GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_11);}
u8 inline main_hatch_in2(){return !GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_12);}
u8 inline main_hatch_charge_done(){return GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_2);}




u8 inline main_lo_in(){	return GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_5);}
u8 inline main_soe_in(){return GPIO_ReadInputDataBit(GPIOD,GPIO_Pin_2);}




int main(void)
{


 	SystemInit();

    	current_state=SETUP;

    	program_runs=main_getRunsfromFlash();
	     /* Valid for Standard Settings with 8MHz Quarz */
	     /* Activation of external Periphery.*/
	    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD|RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC| RCC_APB2Periph_AFIO | RCC_APB2Periph_SPI1, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO | RCC_APB2Periph_ADC1|RCC_APB2Periph_ADC2, ENABLE);
	    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4 |RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM2 | RCC_APB1Periph_USART3| RCC_APB1Periph_DAC, ENABLE);
	    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	    /* Custom Global Variables */

	    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	    rexus_liftoff=FALSE;
		rexus_soe=FALSE;


#ifdef debug
         /*test_PinTest(); */
    	//test_i2c();
    	/*test_pid();*/
    	/*test_dac();*/
	    /* test_spi_adc(); */
	   // test_control();
	//    test_output();
	 //   test_adc();

	 //   test_sensor_gains();
#endif


		stm32_usart_init();
	    stm32_spi_init();

	    printf("\r\n.-Start of Program-. (MB#%u)",MAINBOARD_ID);

	/* --------------------TODO: Es lohnt sich manchmal zum DEBUGGEN alle timer auszuschalten !
	 * Aber denn beachten das es bei der SD Card h�ngen wird. Also das sollte man mit auskommentieren----------------- */

#ifdef useTimers
        init_Timer();
#endif
        init_SysTick();
	 // init_Crystal();
      exti_init();
	  stm32_adc_init();
//	  I2C1_I2C2_BUS_Init();
	  I2C1_LowLevel_Init();
	  fipex_init_sdcard();

#ifdef useFipex
	  fipex_init();
#endif

	  main_bat_off();
	  main_hatch_off();
	  main_hatch_charge_off();


while(1)
    {
   

     if (scheduler_100ms_request==TRUE)
     {
#ifdef useFipex
      fipex_scheduler();
#endif
      scheduler_100ms_request=FALSE;
     }


    /* Keep the Optimizer from Removing this */
    }
}


/* ---------------- Overview of all Timers and their settings -------------
 * Name				Time		Priority
 * SysTick 			10ms        Low
 * fipex_scheduler  100ms       Low
 * TIM4    			 1s         High
 * TIM3             0.1ms       High (Scheduler Overviewing Tasks)
 *
 */


void SysTick_Handler(void){
/* Currently set to 10 ms handles */

static uint16_t SysTickCounter=0;

/*------- Run each ------------ */
SysTickCounter++;
#ifdef useFatFS
disk_timerproc();
#endif

if ((current_state==IDLE)&&(main_soe_in()==Bit_RESET)){
	current_state=HEATING_ONLY;
	main_bat_on();
}

if(current_state!=SETUP) {
	//fipex_i2c_timerproc();
	main_hatch_trigger_proc();
}
/*------- Run each 2nd------------ */


/*------- Run each 10.th------------ */

if (((SysTickCounter%10)==0)&&(current_state!=SETUP)){
	/*After the initialization is finished call this every 100ms */

	if (scheduler_100ms_request==TRUE){
		/* Requesting a Scheduler although the previous one is not finished! */
		scheduler_100ms_request_timeout++;
		main_error(e_timeout);
	}

	scheduler_100ms_request=TRUE;

	}
/*------- Run each 100.th------------ */

if (((SysTickCounter%10)==0)&&(current_state!=SETUP)){
		/* About every 1 second. For the LCD which is very slow */

	if ((SysTickCounter %50) == 0){
		main_showLedStatus();

	} else {main_led_status_off();}

	}


return;
}


int main_getRunsfromFlash(){
/* This function retrieves a number from flash memory, increases it by 1, and saves it.
 * This data is stored permanently.
 */
	uint16_t VarValue = 0;
	/* Virtual address defined by the user: 0xFFFF value is prohibited */

	FLASH_Unlock();

	VarValue=*((uint16_t *) 0x0801F800);
	VarValue++;
	FLASH_ErasePage((uint32_t)0x0801F800);
	FLASH_ProgramHalfWord((uint32_t)0x0801F800, VarValue);
	FLASH_ProgramHalfWord((uint32_t)0x0801F804, VarValue);
	FLASH_Lock();

	return VarValue;
}

void main_stopAll(){
	/* Final Function to stop all measurement and shutdown the SD Card for safety */
    /* TODO: Disable all the critical Timers */
	    current_state=STOP;

#ifdef useFatFS
	    f_close(&file_log);
	    f_close(&file_measurements);
	    f_mount(0, NULL);
#endif


main_hatch_charge_off();
main_bat_off();
main_hatch_off();

}







void inline main_showLedStatus(){
	static u8 position=0;

	if (position++ < (u8)current_state) {
		main_led_status_on();
	}else{
		main_led_status_off();
	}

    if(position>10) {position=0;}
}



void inline main_hatch_trigger_on(){
/* The signal will be set for a certain amount of time. */
 	main_hatch_on();
    fipex_status_hatch_counter=150; /* x*10ms */
    printf("\r\n~CMD. HATCH #ON");
}

void inline main_hatch_trigger_proc(){
	if(fipex_status_hatch_counter!=0){

		if(--fipex_status_hatch_counter==0){
			printf("\r\n~HATCH #OFF");
			main_hatch_off();


			/* Dieser Ort eignet sich für Fehlerabfrage ob die Hatch offen ist
			if (main_hatch_in1()==0) main_error(e_hatch);
			*/

		}

	}
}

void inline main_error(eErrorFlags argError){
setFlag(&fipex_ErrorFlags, argError);

}



void inline main_getStatusFlags(){
setFlagValue(&fipex_StatusFlags,s_i_lo,!main_lo_in());
setFlagValue(&fipex_StatusFlags,s_i_soe,!main_soe_in());
}

void inline main_getHatchFlags(){
//setFlagValue(&HatchFlags,h_i_in1,main_hatch_in1());
setFlagValue(&fipex_HatchFlags,h_i_in2,main_hatch_in2());
//setFlagValue(&HatchFlags,h_i_doneCharge,main_hatch_charge_done());
}



/* Litle Helper Functions */

inline u8 main_usart_parse09(u8 argData ){
	/* Read Integer 0-9 at one Position */
	u8 temp;
	temp= argData-48;
	if(temp<10)
	{ return temp;}
	return 255;
}
	inline double main_usart_parseFloat(u8 *data, u8 argPos){
	/* Read Integer 0-9 at one Position */
	 u8 i,tmp, posComma=0, posSemicolon=0;
	 double value=0;

		for(i=argPos; i<16;i++) {
        if(data[i]==',' ||data[i]=='.' )posComma=i;
        if(data[i]==';' )posSemicolon=i;
		}

		/* XXX.---; */
		for(i=argPos;i<(posComma);i++){
			tmp=main_usart_parse09(data[i]);
			if(tmp!=255){
			value+= tmp*__pow(10,posComma-i-1);
		} else{ printf("\r\n #Fault. Invalid Float Input. Use XXX.YYY;");break;};
		}
		/* ---.YYY; */
		for(i=posComma+1;i<(posSemicolon);i++){
					tmp=main_usart_parse09(data[i]);
					if(tmp!=255){
					value+= (double)tmp/__pow(10,i-posComma);
				} else{ printf("\r\n #Fault. Invalid Float Input. Use XXX.YYY;");break;};
				}
	return value;
}

/* Its 1 a.m. I wonder if anybody ever going to read this again. 
	All the work and no play make jack a dully boy.
*/



void main_usart_rx_handler(unsigned char *data){
	/* Check the type of command */



	switch (data[0]) {
	case 'd':{
		  printf("\r\n.--Built %s@%s  --.", __DATE__,__TIME__);
		  printf("\r\n.--Mainboard %u  --.", MAINBOARD_ID);
		#ifdef debug
		  printf("\r\n.----DEBUG MODE------.");
		#endif

	}break;
	case 'f':{
		/*Get Info from one Fipex PID Device */
		u8 tmp=main_usart_parse09(data[1]);
		if (tmp>=0 || tmp<10){
		printf("\r\n(%u) TV=%f, SV=%i, PV=%f,P=%f(m),I=%f(m),D=%f(m)",(unsigned int) tmp,_sDevices[tmp].targetValue,_sDevices[tmp].setValue,_sDevices[tmp].processValue,_sDevices[tmp].PID.P,_sDevices[tmp].PID.I,_sDevices[tmp].PID.D);
		}else{
		printf("\r\n Invalid Channel Input.");
		}
	}break;

	case 'F':{
		/*Get Info from one Fipex PID Device */
		u8 tmp=main_usart_parse09(data[1]);
		if (tmp>=0 || tmp<6){

			 float  tmp3=main_usart_parseFloat(data,3);

				 switch(data[2]){
				 case 'T':{_sDevices[tmp].targetValue=tmp3;}break;
				 case 'P':{_sDevices[tmp].PID.P=tmp3;}break;
				 case 'I':{_sDevices[tmp].PID.I=tmp3;}break;
				 case 'D':{_sDevices[tmp].PID.D=tmp3;}break;
				 default:  printf("\r\n Invalid Setting Input (PIDT)."); break;
				 }

		}else{
			printf("\r\n Invalid Channel Input(0-5).");
		}
	}break;


	case 'e':{

	//printf("\r\nADC:%u|%u|%u|%u",fipex_adc_values[0],fipex_adc_values[1],fipex_adc_values[2],fipex_adc_values[3]);
	//printf("\r\nADC1-4(mV):%u|%u|%u|%u",(fipex_adc_values[0]*805)/1000,(fipex_adc_values[1]*805)/1000,(fipex_adc_values[2]*805)/1000,(fipex_adc_values[3]*805)/1000); /*entspricht *3300mv/4095 */
	//printf("\r\nTEMP:%iC|%iC|%iC|%iC",fipex_temp_values[0],fipex_temp_values[1],fipex_temp_values[2],fipex_temp_values[3]);
	//printf("\r\nTIMEOUTS: %u@10ms, %u@100ms",scheduler_10ms_request_timeout,scheduler_100ms_request_timeout);
	}break;


	case 'L':{

		if(data[1]=='0') {fipex_time_lo=0;}
	}break;
	case 'S':{

		switch(data[1]){
		case '0':current_state=SETUP; break;
		case '1':current_state=IDLE; break;
		case '2':current_state=HEATING_ONLY; break;
		case '3':current_state=MEASURE; break;
		case '4':current_state=FLIGHT;break;
		case '5':current_state=STOP;break;
		case '6':current_state=TEST;break;
		case '7':current_state=TEST_FEEDBACK;break;
		}
	}break;

	case 'B':{
			if(data[1]=='1')main_bat_on();
			if(data[1]=='0')main_bat_off();

		}break;

	case 'C':{
			if(data[1]=='1')main_hatch_charge_on();
			if(data[1]=='0')main_hatch_charge_off();

		}break;

	case 'H':{
			if(data[1]=='T')main_hatch_trigger_on();
			if(data[1]=='1')main_hatch_on();
			if(data[1]=='0')main_hatch_off();

		}break;


	default: printf("\r\n Unknown Input.");
	}
	return;
	}

















