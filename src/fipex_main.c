﻿/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		06.02.2013
 *
 *
 * -------------------------------------------------------------
 *			File Hierachy
 * -------------------------------------------------------------
 *			STM32 related: /syscalls /stmlib /semihosting /cmsis_boot /cmsis
 *			HAL			 : /HAL LIBS
 *							SD Card: ff, diskio,ffconf
 *							ADC(STM):stm_32adc
 *			Project Files: Interrupt Handler: m_interrupts (tbr)
 *			               Init Routines    : m_init (tbr)
 *
 * -------------------------------------------------------------
 *          Experiment Data
 *  -------------------------------------------------------------
 *  14*2Byte ADC Value
 *  4*2 Byte ADC2 Values
 *
 *  6*2 Byte TEMP Values
 *  1*10 Byte Status
 *  3*2*2 Byte Heating Data (U,I)
 *  3*3*2 Byte Heating Data (U,I,Gain)
 *  ~50 Byte Infostring
 *  ---------------------
 *
 * -------------------------------------------------------------
 *          Comments Section
 *  -------------------------------------------------------------
 *  MB11,12 Fixes:
 *	Pin Switch: Hatch Charge <> Hatch Done.
 *	Hatch1<>Hatch2
 *	Using Hatch2 only. (tPlace Label Hatch1)
 */


#include "includes.h"

/* Public Experiment variables -----------------------------------------------*/

volatile uint32_t fipex_ticks=0;
volatile uint32_t fipex_time_sys=0;
volatile uint32_t fipex_time_lo=0;



uint8_t fipex_status_hatch_counter=0;

/* Binary Coded Flags */
uint8_t fipex_HatchFlags	=0;
uint8_t fipex_StatusFlags	=0;
uint8_t fipex_ErrorFlags	=0;


/* Private Functions Declaration to avoid Implicit Declarations */
		void _fipex_retrieve_ADC_values();
		void _fipex_retrieve_TEMP_values();
		void _fipex_retrieve_PRESSURE_values();
		


		void _fipex_sendPackage_Devices();
		void _fipex_sendPackage_Device(u8 i);
		void _fipex_sendPackage_Pressure();
		void _fipex_sendPackage_Status();
		void _fipex_sendPackage_Power();
		void _fipex_sendPackage_Temperatures();

		void _fipex_file_writeMeasurements();

/* Private Variables Declaration to avoid Implicit Declarations */

		uint16_t fipex_adc_values[14]={}; // Reference Value 3.300V
		//extern uint16_t spi_adc_values[5]; // Reference Value 4.096V

		/* We definetely have up to 4*12 Bit Values per Sensor at 3 Sensors. Resulting in 24byte/192bits. Thus resulting in 24+8=32 byte Packages. */
		/* To avoid splitting of Data packages, Size of Data Packages will be increased! */

		
		int16_t fipex_temp_values[6]; //centigrade
		uint16_t fipex_pressure_values[2]; //e-6 Bar

		











/* This is the Fipex Returning Scheduler Atomic Tick. It will get triggered about every 100ms by the System Scheduler
 * @caller SysTick
 */
void fipex_scheduler(){
	/* Create a counter for tasks which are to performed only every now and then */
	static u16 fipex_scheduler_counter=0;
	fipex_scheduler_counter++;



    /* Task 0. Read out ADC / Temp / into the Buffers here. Start a new ADC conversion if wanted */
	fipex_scheduler_perfmon_starttask(1);
					_fipex_retrieve_ADC_values();
					spi_adc_sampleAll();

					/* This function causes a delay when it can not connect to a device and read values. */
					if((fipex_scheduler_counter%5)==0){
					_fipex_retrieve_TEMP_values();
					}

					/* Occasionally send a Beacon which shows the MB ID when Idle */
					if(((fipex_scheduler_counter%200)==0)&&(current_state==IDLE)){
						printf("\r\n[MB%u][Run%u]Idle                               ", MAINBOARD_ID,(unsigned int)program_runs);
					}


					_fipex_retrieve_PRESSURE_values();
					main_getHatchFlags();
					main_getStatusFlags();

	/* Task 1. Print & Pack  the Packages to the USART Console.  */
	fipex_scheduler_perfmon_starttask(2);
					//usart_sendPackage24(fipex_adc_values,MSGID_ADC1A);
					//usart_sendPackage24(&fipex_adc_values[7],MSGID_ADC1B);
					//usart_sendPackage24(fipex_adc2_values,MSGID_ADC2);
					/*Considerations. When packing this for CSV. 1 uint16 = 4 characters + delimiter. So we can pack MAX 4 values per package */
					//usart_sendPackage24((uint16_t*) fipex_temp_values,MSGID_TEMP0);
				

					_fipex_sendPackage_Devices();
					_fipex_sendPackage_Pressure();
					_fipex_sendPackage_Status();
					_fipex_sendPackage_Power();

#ifdef debugPID

	    // One of the sensor can be examined in detail. Therefore this static variable can be defined. If debug is enabled, the values will be transmitted for each cycle.
						const u8 tmp_detailedSensor=3;
							_fipex_sendPackage_Device_PID(tmp_detailedSensor);
#endif

					if((fipex_scheduler_counter%5)==0)
					_fipex_sendPackage_Temperatures();

					



	/* Task 2. Write/Sync Everthing to the SD Card                    */
	fipex_scheduler_perfmon_starttask(3);
	
	#ifdef useFatFS
	
					_fipex_file_writeMeasurements();
					/* Sync the files */
					if ((fipex_scheduler_counter)%50==0)
										{
										f_sync(&file_measurements);
										}
					
					  if ((fipex_scheduler_counter+2)%50)
										{
										f_sync(&file_log);
										}
					
					
				   

					
					/* TODO: Catch the result if the writing was sucessfull! */
	                	/* TODO: Also here the writing process introduces a possible lag which will have the Scheduler Timeout. Consider better write leveling */
					
#endif
					    
	                	
	/* Task 4. Call the occasional tasks                         */
	fipex_scheduler_perfmon_starttask(4);





	/* Mandatory Finishing Tasks */

    /*
     * This function refers to the internal Watchdog Timer. Shall not be used until final implementation
     * IWDG_ReloadCounter();
     */


	/* Optional Finishing Tasks */
    /* Put current task output to Logbook! */



#ifdef debug

	if(fipex_scheduler_counter%10==0){
	printf("\r\n[State=%i][# %u]",current_state, (unsigned int)fipex_scheduler_counter);
	printf("[T %us][LO %us]",(unsigned int)fipex_time_sys,(unsigned int)fipex_time_lo);

	printf("\r\nTEMP:%iC|%iC|%iC|%iC",(int) fipex_temp_values[0],(int)fipex_temp_values[1],(int)fipex_temp_values[2],(int)fipex_temp_values[3]);
	printf("\r\nTASKS:%u(%u)|%u(%u)|%u(%u)|%u(%u)|%u(%u)",(unsigned int)_scheduler_tasktimes[0],(unsigned int)_scheduler_tasktimes_10s[0],(unsigned int)_scheduler_tasktimes[1],(unsigned int)_scheduler_tasktimes_10s[1],(unsigned int)_scheduler_tasktimes[2],(unsigned int)_scheduler_tasktimes_10s[2],(unsigned int)_scheduler_tasktimes[3],(unsigned int)_scheduler_tasktimes_10s[3],(unsigned int)_scheduler_tasktimes[4],(unsigned int)_scheduler_tasktimes_10s[4]);
	printf("\r\nTIMEOUTS: %u@10ms, %u@100ms",(unsigned int)scheduler_10ms_request_timeout,(unsigned int)scheduler_100ms_request_timeout);
	printf("\r\n-----------------------------");
	}
#endif



	// Start the next sampling cycle.
	spi_adc_sampleAll();
	fipex_scheduler_perfmon_starttask(0);

return;
};




inline void _fipex_sendPackage_Infostring_Add(char parChar){
	static unsigned char msg_buffer_infostring[24]={};
	static uint8_t msg_buffer_infostring_pos=0;


	msg_buffer_infostring[msg_buffer_infostring_pos++]=parChar;
	if(msg_buffer_infostring_pos==24){
		stm32_usart_protocol_sendPackage(msg_buffer_infostring,MSGID_INFOSTRING,24);
		msg_buffer_infostring_pos=0;
	}


}


inline void _fipex_sendPackage_Status(){
	/*Transmit State as Binary Byte Package */
	clear_buffer();
	msg_buffer[0]=(u8) current_state;
	msg_buffer[1]=(u8) (fipex_time_sys>>8);
	msg_buffer[2]=(u8)fipex_time_sys;
	msg_buffer[3]=(u8)(fipex_time_lo>>8);
	msg_buffer[4]=(u8)fipex_time_lo;
	msg_buffer[5]=(u8) fipex_HatchFlags;
	msg_buffer[6]=(u8) fipex_StatusFlags;
	msg_buffer[7]=(u8) fipex_ErrorFlags;
	//stm32_usart_protocol_sendPackage24(msg_buffer,MSGID_STATUS0);

	stm32_usart_protocol_sendPackage(msg_buffer,MSGID_STATUS0,8);

}
inline void _fipex_sendPackage_Device(u8 i){
	clear_buffer();
	msg_buffer[0]=(u8) _sDevices[i].Device;
	msg_buffer[1]=(u8)( _sDevices[i].ADC_U>>8);
	msg_buffer[2]=(u8) _sDevices[i].ADC_U;
	msg_buffer[3]=(u8) (_sDevices[i].ADC_I>>8);
	msg_buffer[4]=(u8) (_sDevices[i].ADC_I);
	
	msg_buffer[5]=(u8) _sDevices[i].Gain;

	msg_buffer[6]=(u8) (_sDevices[i].setValue>>8);
	msg_buffer[7]=(u8) (_sDevices[i].setValue);

	stm32_usart_protocol_sendPackage(msg_buffer,MSGID_DEVICE,8);
}

inline void _fipex_sendPackage_Device_PID(u8 i){
	clear_buffer();
	msg_buffer[0]=(u8) _sDevices[i].Device;
	
	uint16_t tmp;
	tmp = double_to_u16(_sDevices[i].PID.P);
	msg_buffer[1]=(u8) (tmp>>8);
	msg_buffer[2]=(u8) (tmp);
	
	tmp = double_to_u16(_sDevices[i].PID.I);
	msg_buffer[3]=(u8) tmp>>8;
	msg_buffer[4]=(u8) tmp;
	
		tmp = double_to_u16(_sDevices[i].PID.D);
	msg_buffer[5]=(u8)( tmp>>8);
	msg_buffer[6]=(u8)( tmp);

	
	tmp = double_to_u16(_sDevices[i].targetValue);
	msg_buffer[7]=(u8) (tmp>>8);
	msg_buffer[8]=(u8) (tmp);
	
	tmp = double_to_u16(_sDevices[i].processValue);
	msg_buffer[9]=(u8) (tmp>>8);
	msg_buffer[10]=(u8) (tmp);
	
		tmp = double_to_u16(_sDevices[i].setValue);
	msg_buffer[11]=(u8) (tmp>>8);
	msg_buffer[12]=(u8) (tmp);

	stm32_usart_protocol_sendPackage(msg_buffer,MSGID_DEVICE_PID,13);
}


inline void _fipex_sendPackage_Devices(){
int i;
	for(i=0; i<6;i++)
	{
	_fipex_sendPackage_Device(i);
	}
}




inline void _fipex_sendPackage_Pressure(){
	clear_buffer();
	msg_buffer[0]=(u8) 0xFF & (spi_adc_values[0]>>8);
	msg_buffer[1]=(u8) 0xFF & spi_adc_values[0];
	msg_buffer[2]=(u8) 0xFF & (spi_adc_values[1]>>8);
	msg_buffer[3]=(u8) 0xFF & spi_adc_values[1];
		stm32_usart_protocol_sendPackage(msg_buffer,MSGID_PRESSURE,4);

}

inline void _fipex_sendPackage_Power(){
	clear_buffer();
	msg_buffer[0]=(u8) (spi_adc_values[2]>>8);
	msg_buffer[1]=(u8) (spi_adc_values[2]);
	msg_buffer[2]=(u8) (spi_adc_values[3]>>8);
	msg_buffer[3]=(u8) (spi_adc_values[3]);
	msg_buffer[4]=(u8) (spi_adc_values[4]>>8);
	msg_buffer[5]=(u8) (spi_adc_values[4]);
//	msg_buffer[6]=(u8) (spi_adc_values[5]>>8);
//	msg_buffer[7]=(u8) (spi_adc_values[5]);
	stm32_usart_protocol_sendPackage(msg_buffer,MSGID_POWERS,8);

}

inline void _fipex_sendPackage_Temperatures(){
	clear_buffer();
	msg_buffer[0]= (fipex_temp_values[0]>>8);
	msg_buffer[1]= fipex_temp_values[0];
	msg_buffer[2]= (fipex_temp_values[1]>>8);
	msg_buffer[3]= fipex_temp_values[1];
	msg_buffer[4]= (fipex_temp_values[2]>>8);
	msg_buffer[5]= fipex_temp_values[2];
	msg_buffer[6]= (fipex_temp_values[3]>>8);
	msg_buffer[7]= fipex_temp_values[3];
	msg_buffer[8]= (fipex_temp_values[4]>>8);
	msg_buffer[9]= fipex_temp_values[4];
	msg_buffer[10]= (fipex_temp_values[5]>>8);
	msg_buffer[11]= fipex_temp_values[5];
	stm32_usart_protocol_sendPackage(msg_buffer,MSGID_TEMP0,12);

}




void _fipex_retrieve_ADC_values(){
/*ADCBuffer Structure
 *  IN11,IN10 | IN13, IN12 | ...
 *
 * */




	fipex_adc_values[0] = stm32_adc_ADCBuffer[0]&0x0000FFFF;
	fipex_adc_values[1] = stm32_adc_ADCBuffer[0]>>16;
	fipex_adc_values[2] = stm32_adc_ADCBuffer[1]&0x0000FFFF;
	fipex_adc_values[3] = stm32_adc_ADCBuffer[1]>>16;
	fipex_adc_values[4] = stm32_adc_ADCBuffer[2]&0x0000FFFF;
	fipex_adc_values[5] = stm32_adc_ADCBuffer[2]>>16;
	fipex_adc_values[6] = stm32_adc_ADCBuffer[3]&0x0000FFFF;
	fipex_adc_values[7] = stm32_adc_ADCBuffer[3]>>16;
	fipex_adc_values[8] = stm32_adc_ADCBuffer[4]&0x0000FFFF;
	fipex_adc_values[9] = stm32_adc_ADCBuffer[4]>>16;
	fipex_adc_values[10] = stm32_adc_ADCBuffer[5]&0x0000FFFF;
	fipex_adc_values[11] = stm32_adc_ADCBuffer[5]>>16;
	fipex_adc_values[12] = stm32_adc_ADCBuffer[6]&0x0000FFFF;
	fipex_adc_values[13] = stm32_adc_ADCBuffer[6]>>16;




return;
};

void _fipex_retrieve_TEMP_values(){
	/* This function collects the Temperatures from the various sensors. Converts to �C
	 * Sensor 1 - the internal STM sensor (ADC16 Value)
	 */

	/* Temperature Sensore inside STM32
	 * Characteristics: V@25�C = 1.43V
	 * dV=4.3mv/K                */

	/* Testcode f�r Temperaturmessung */
		uint16_t value = fipex_adc_values[12];
		/* Print on Debug Line */

       //STM Internal Temp Sensor
		double valued =(25-((double)value*3.3/4096-1.43)*1/0.0043) * 100;

	    fipex_temp_values[0]= (s16)  valued;



	    if(current_state>2){
	    	/* Do not measure the temperature when in Idle Mode.
	    	 * Cause: I2C TEMOD Pt1000 can cause a reverse-drain when SDA/SCL is pulled down
	    	 */

	    fipex_temp_values[1]= I2C_read_PT1000();
	    fipex_temp_values[2]= I2C_read_LM75( 0b000);


	    }

	return;
}





void _fipex_retrieve_PRESSURE_values()
{
	/* We could perform conversion to actual pressure values. Currently the mV is more efficient */
	fipex_pressure_values[0]= ((uint32_t)spi_adc_values[0]*4096)/65535;
	fipex_pressure_values[1]=((uint32_t)spi_adc_values[1]*4096)/65535;

}

#ifdef useFatFS

inline void _fipex_file_writeHeader(){
	 /* Make the output files look nice by adding headers. */

	int cc=0;


		cc+=				f_printf(&file_measurements,"TimeSys[1/10s];TimeLO[1/10s];State;");
		cc+=				f_printf(&file_measurements,"StatusFlags;HatchFlags;ErrorFlags;");

						unsigned int i;
						// Print Pressures
						for(i=0;i<2;i++){
		cc+=					f_printf(&file_measurements,"P%U[mV];",i);
							}

						// Print Device Data
						for(i=0;i<6;i++){
		cc+=				f_printf(&file_measurements,"(%U)ADC_U;ADC_I;Gain;SetValue;",i);

						}

						// Print Temperatures
						for(i=0;i<6;i++){
		cc+=				f_printf(&file_measurements,"Temperature%U[1/100 C];",i);
						}

			if(cc<0) {
				/* cc contains the written bytes. when failed, this function returns -1 */
				setFlag(&fipex_ErrorFlags,e_sdcard);
			}


}

inline void _fipex_file_writeMeasurements(){
	static __attribute__((__unused__)) int bw;

	/* Experimental Code for Write to SD Card
	int bw;
	snprintf(text,(size_t)255,"SysTime: %d|",(int)time_sys);
	f_write(&file_log, text, 15, &bw);

	snprintf(text,(size_t)255,"T1 %d �C\r\n",(int)temp);
 	f_write(&file_log, text, 15, &bw); */

// ##  Write raw ADC Values all ##
				//	clear_buffer();
				//	snprintf(msg_buffer,24,"ADC1;%u;%u;%u;%u;%u",(unsigned int) time_sys,fipex_adc_values[0],(unsigned int)fipex_adc_values[1],(unsigned int)fipex_adc_values[2],(unsigned int)fipex_adc_values[3]);
				//	f_write(&file_measurements, "\r\n", 2, &bw);
				//	f_write(&file_measurements, msg_buffer, 24, &bw);

					//Supports B Binary, D Decimal , U Unsigned Decimal
int cc=0;


	cc+=				f_printf(&file_measurements,"\r\n%U;%U;%U;",fipex_time_sys,fipex_time_lo,current_state);
	cc+=				f_printf(&file_measurements,"%U;%U;%U;",fipex_StatusFlags,fipex_HatchFlags,fipex_ErrorFlags);

					int i;
					// Print Pressures [mV]
					for(i=0;i<2;i++){
	cc+=					f_printf(&file_measurements,"%U;",fipex_pressure_values[i]);
						}

					// Print Device Data
					for(i=0;i<6;i++){
	cc+=				f_printf(&file_measurements,"%U;%U;%U;%U;",_sDevices[i].ADC_U,_sDevices[i].ADC_I,_sDevices[i].Gain,_sDevices[i].setValue);
					}

					// Print Temperatures
					for(i=0;i<6;i++){
	cc+=				f_printf(&file_measurements,"%D;",fipex_temp_values[i]);
					}

		if(cc<0) {
			/* cc contains the written bytes. when failed, this function returns -1 */
			setFlag(&fipex_ErrorFlags,e_sdcard);
		}

}






#endif



/* This is the Fipex Initialization of GPIO Routine. It will get triggered by fipex_init() 
 * @caller SysTick */

void  fipex_Init_GPIO(void){

	GPIO_InitTypeDef   GPIO_InitStructure;


	
/* Main Hardware Outs & Ins */
// Outputs-----------------------------------------------

//
	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;

	  
//Hatch on
	  GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_10;
	  GPIO_Init(GPIOA, &GPIO_InitStructure);

 // Hatch Charge,Led Status, Led SOE
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2 | GPIO_Pin_8 | GPIO_Pin_9;
	  GPIO_Init(GPIOB, &GPIO_InitStructure);
// Hatch Charge, Bat on
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_12 | GPIO_Pin_13;
	  GPIO_Init(GPIOC, &GPIO_InitStructure);
	  
// Programmable Gain Array  
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6| GPIO_Pin_7| GPIO_Pin_8| GPIO_Pin_9| GPIO_Pin_10| GPIO_Pin_11;
	  GPIO_Init(GPIOC, &GPIO_InitStructure);
	  

// Inputs ------------------------------------------------
	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;

// External 32kHz Crystal
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_14 | GPIO_Pin_15;
	  GPIO_Init(GPIOA, &GPIO_InitStructure);
/*
	  // Hatch done, Liftoff
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2 | GPIO_Pin_5  ;
	  GPIO_Init(GPIOB, &GPIO_InitStructure); */


	  	  // Hatch done, Liftoff
	  	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5  ;
	  	  GPIO_Init(GPIOB, &GPIO_InitStructure);
	  
// Hatch in 1&2
	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11 | GPIO_Pin_12  ;
	  GPIO_Init(GPIOA, &GPIO_InitStructure);
//SOE

	  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2  ;
	  GPIO_Init(GPIOD, &GPIO_InitStructure);



	  }

void fipex_init(){

fipex_Init_GPIO();
fipex_control_init();
fipex_setGain(0);
fipex_setGain(1);
fipex_setGain(2);

/* Print out some additional Informational Output to Semihosting                        */


  printf("\r\n.--Built %s@%s  --.", __DATE__,__TIME__);
  printf("\r\n.--Run Nr.         %u          --.", (unsigned int) program_runs );

  
 #ifdef useFatFS
  _fipex_file_writeHeader();
  #endif
  
  
#ifdef debug
		printf("\r\n.----DEBUG MODE------.");
#endif

		current_state=IDLE;

#ifndef debug
 // Check if the LO is already present. If so - go into a Measuring Mode directly. Measure it for several times to be sure. (Debounce)
        u16 tmpCount = 1000;
		while((main_lo_in()==Bit_RESET)&&(tmpCount--)){
			if(tmpCount==0){
			current_state = FLIGHT;
			}
		}
#endif

return;
}

void fipex_init_sdcard(){

#ifdef useFatFS
        FRESULT res;
	 	res = f_mount(0, &fs[0]);

	 	/* Create File Names according to Run Number. Actually need to use seperate char array for each name since they dont get clear meanwhile */
	 	/* How i wasted 2 hours of my life. File names only 8 characters. */

	 	TCHAR filename[8+1+3];

	 	snprintf(filename ,(size_t)sizeof(filename),"%ulog.txt",(unsigned int)program_runs);


			res = f_open(&file_log,filename , FA_CREATE_ALWAYS | FA_WRITE);
			if (res) {printf("\r\n#Fault.SD Card Error File 1! Result %i",res);main_error(e_sdcard);}


			snprintf(filename,(size_t)sizeof(filename),"%umeas.csv",(unsigned int) program_runs);
			res = f_open(&file_measurements,filename , FA_CREATE_ALWAYS | FA_WRITE);
			if (res){printf("\r\n#Fault.SD Card Error File 2! Result %i",res);main_error(e_sdcard);}

#endif
}




void fipex_i2c_timerproc(){

/* This will perform the I2C Reading timed at each SysTick */
	static u8 SysTicks=0;
	++SysTicks;
switch(SysTicks%8){
}
}




