#include "includes.h"
/******************************************************************************/
/*            STM32F10x Peripherals Interrupt Handlers                        */
/******************************************************************************/


void I2C2_EV_IRQHandler(void){
 // i2c_handleEventInterrupt();
}

void I2C2_ER_IRQHandler(void){
 // i2c_handleErrorInterrupt();
}







/******************************************************************************/
/*            STM32F10x Fault Handlers                                        */
/******************************************************************************/

void MemManage_Handler(void){
printf(".fault.:MemManage");


while(1);
}



void BusFault_Handler(void){
printf(".fault.:BusFault");


while(1);
}

void UsageFault_Handler(){


printf(".fault.:UsageFault");

while(1);
}

void DebugMon_Handler(){



printf(".fault.:DebugMon\n\r");

while(1);
}



