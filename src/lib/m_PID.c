/* Implementation of PID controller for float and double precision */
#include "m_PID.h"

double PID_calc (struct sPID* argPID, double sp, double pv)
{
double err_old=argPID->lasterror;
double err=sp-pv;
argPID->lasterror=err;

argPID->integrator+=err_old;

/* ----Anti Windup Reset ---*/
if (argPID->integrator> 9000){
	argPID->integrator=0;
#ifdef debug
printf("\n\r #debug fault.PID Integrator AWR");
#endif
} else if (argPID->integrator< -9000){
	argPID->integrator=0;
#ifdef debug
printf("\n\r #debug fault.PID Integrator AWR");
#endif
}
return (argPID->P*err+argPID->I*argPID->integrator+argPID->D*(err-err_old));
}





float PIDf_calc(struct sPIDf* argPID, float sp, float pv)
{

float err_old=argPID->lasterror;
float err=sp-pv;
argPID->lasterror=err;

argPID->integrator+=err_old;

/* ----Anti Windup Reset ---*/
if (argPID->integrator> 2000){
	argPID->integrator=0;
#ifdef debug
printf("\n\r #debug fault.PID Integrator AWR");
#endif
} else if (argPID->integrator< -2000){
	argPID->integrator=0;
#ifdef debug
printf("\n\r #debug fault.PID Integrator AWR");
#endif
}
return (argPID->P*err+argPID->I*argPID->integrator+argPID->D*(err-err_old));
}
