/* Private Typedef Declaration to avoid Implicit Declarations */
		enum MSG_BYTE {

					   MSGID_INFOSTRING=0x00, /* - variable */
					   MSGID_ACK=0x01, /* - Response: Acknowledged */
					   MSGID_NACK=0x02,/* - Response: Not Acknowledged */
					   SYNC=0x16,

/* Moxa Protocol 0xA0 - 0xE0 */

					   MSGID_ADC1A=0xA0, /* 7 ADC1_0-6 x 2 Byte */
					   MSGID_ADC1B=0xA1, /* 7 ADC1_7-13 x 2 Byte */
					   MSGID_ADC2=0xA2,  /* 4 ADC2_0-3 x 2 Byte */
					   MSGID_ADC_CSV=0xAF, /* -variable - */
					   MSGID_TEMP0=0xB0, /* 6 TEMP x 2 Byte */
					   MSGID_TEMP1=0xB1, /* -void - */
					   MSGID_TEMP2=0xB2, /* -void - */
					   MSGID_TEMP_CSV=0xBF, /* -variable - */
					   MSGID_DEVICE=0xC0,  /* 3 Sens1-3 x 3 x 2 Byte */
					   MSGID_DEVICE_PID = 0xC1,
					   MSGID_POWERS=0xD0,   /* 3*2 Byte: U_bat|I_bat|I_rexus */
					   MSGID_PRESSURE=0xDA, /* 2*u16: Pressure VSP62|JUMO */

					   MSGID_STATUS0=0xE0, /* TIME_LO, TIME_SYS, STATE, HATCH */
					   MSGID_STATUS1=0xE1, /* void */
/* BST Protocol 0x10 - 0x2F */
					   
                      			   MSGID_MOTOR_POS=0x10,
                      		           MSGID_MOTOR_CMD=0x11,
                                           MSGID_MOTOR_CMD_ANGLE=0x12,
                                           MSGID_MOTOR_CMD_STEPS=0x13,
                                           MSGID_MOTOR_CMD_TARGET=0x14,
                                           MSGID_MOTOR_CMD_RATE=0x15,

                      			   MSGID_BST_ACCELLERATION_XYZ=0x20,
                      		           MSGID_BST_TORQUE_XYZ=21,


		};


#define RINGBUFFER_SIZE 8192


/* Public Function Prototypes */



void srm32_usart_protocol_sendPackage16(unsigned char *data, u8 msgid_code);
void stm32_usart_protocol_sendPackage24(unsigned char *text, u8 msgid_code);
void stm32_usart_protocol_sendPackage(unsigned char *text, u8 par_ID, u8 par_length);

//void RS232_sendString(char *text, uint8_t *length);

void stm32_usart_init();

/* Private Function Prototypes */
void _stm32_usart_protocol_sendByteArray(char *text, uint8_t length);


void init_RS232();


void usart_queue_add(uint8_t data);
void usart_queue_send();

u16 crc16_ccitt(u16 crc, u8 ser_data);
u16 crc16(const u8 *p, int len);
