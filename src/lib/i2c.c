 /*  -------------------------------------------------------------
 *          STM32F1 I2C
 *  -------------------------------------------------------------
 *
 *
 *	   
 * -------------------------------------------------------------
 *          Comments Section
 *  -------------------------------------------------------------
 *  Author: Schultze
 *  24.04.2014
 */
#include <stdint.h>
#include "i2c.h"
#include "includes.h"
#include "stm32f10x_gpio.h"

int I2Cerror, I2Cerrorcount;

void I2C_delay(void)
{
    Delay_us(2);
}

#define PT1000_ADDR 0x78
#define LM75_ADDR 0x48


s16 I2C_read_PT1000(){

	u16 reading=0;

			I2Cerror=0;
	        I2C1_Start();
	        I2C1_SendByte(((PT1000_ADDR<<1) | 0x01));//fe-1(read)
	        I2C1_WaitAck();
		    reading = (I2C1_ReceiveByte()&0xFF)<<8; //receive
		    I2C1_Ack();

		    reading |= I2C1_ReceiveByte()&0xFF; //receive
		    I2C1_NoAck();

		    I2C1_Stop();

		s16 temp= (-3200 + 100*reading/64);
		return temp;
}

s16 I2C_read_LM75(u8 par_LM75_ID){

	s16 reading=0;
	u8 address = (LM75_ADDR | par_LM75_ID )<<1;
			I2Cerror=0;

		    I2C1_Start();
		    I2C1_SendByte((address));//fe-0(Write)
		    I2C1_WaitAck();
		    I2C1_SendByte(0x00); // Temp Register
		    I2C1_WaitAck();
	        I2C1_Stop();
	        I2C1_Start();
	        I2C1_SendByte((address | 0x01));//fe-1(read)
	        I2C1_WaitAck();
		    reading = (I2C1_ReceiveByte()&0xFF)<<8; //receive
		    I2C1_Ack();

		    reading |= I2C1_ReceiveByte()&0xFF; //receive
		    I2C1_NoAck();
		    I2C1_Stop();

		    reading &= 0xFFE0;
		    reading = reading >> 5;
		    reading = (reading *125) / 10;
		return reading;
}


void I2C1_LowLevel_Init(void){
	GPIO_InitTypeDef    GPIO_InitStructure;

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;       //LED Output Config
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    SDAH();
    SCLH();
}

void I2C1_Start(void)
{
    SDAH();
    SCLH();
    I2C_delay();
    SDAL();
    I2C_delay();
    SCLL();
    I2C_delay();

}

void I2C1_Stop(void)
{
    SCLL();
    I2C_delay();
    SDAL();
    I2C_delay();
    SCLH();
    I2C_delay();
    SDAH();
    I2C_delay();
}

void I2C1_Ack(void)
{
    SCLL();
    I2C_delay();
    SDAL();
    I2C_delay();
    SCLH();
    I2C_delay();
    SCLL();
    I2C_delay();
}


void I2C1_NoAck(void)
{
    SCLL();
    I2C_delay();
    SDAH();
    I2C_delay();
    SCLH();
    I2C_delay();
    SCLL();
    I2C_delay();
}

void I2C1_SendByte(unsigned char SendByte)
{
    //    int8_t i = 8;
    unsigned char i = 8;

    while (i--)
     
    {
        SCLL();
        I2C_delay();

        if (SendByte & 0x80)
        {
            SDAH();
        }

        if (!(SendByte & 0x80))
        {
            SDAL();
        }

        SendByte <<= 1;
        I2C_delay();
        SCLH();
        I2C_delay();
    }

    SCLL();
}

uint8_t I2C1_ReceiveByte(void)
{
 
    unsigned char i = 8;
    unsigned char ReceiveByte = 0;
    uint8_t t;
    uint8_t data;

    SDAH();

    while (i--)
    {
        ReceiveByte <<= 1;
        SCLL();
        I2C_delay();
        SCLH();
        data = 0;

        for (t = 0; t < 8; t++)
        {
            data += GPIO_ReadInputDataBit(I2C_SDA_PORT, I2C_SDA_PIN);
        }

        if (data >= 4)
        {
            ReceiveByte |= 0x01;
        }

    }

    SCLL();
    return ReceiveByte;
}

void I2C1_WaitAck(void)
{
    SCLL();
    I2C_delay();
    SDAH();
    I2C_delay();
    SCLH();
    I2C_delay();

    if (GPIO_ReadInputDataBit(I2C_SDA_PORT, I2C_SDA_PIN) == 1)
    {
        I2Cerror = 1;
        I2Cerrorcount++;
    }

    SCLL();

}

/*
//retrun values: 1 - ok; 1 - error;
uint8_t I2C1_CheckAck(void)
{
    uint8_t ack = 0;

      SCLL();
      I2C_delay();
      SDAH();
      I2C_delay();
      SCLH();
      I2C_delay();
      if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11) == 1)
      {
          ack = 1;
          DEBUG_LEDon();
          I2Cerrorcount++;
      }
      SCLL();

    return ack;
}
*/

