#define SPI_RINGBUFFER_SIZE 64
#define SPI_ADC_CHANNELS 5

/*Public Prototypes */
void stm32_spi_init();
void spi_dac_set(uint8_t argChannel, uint16_t argValue);
void spi_adc_sampleAll();

/* Public Variables */

uint16_t spi_adc_values[SPI_ADC_CHANNELS];

/* Private Enums */

/* Enum Declaration */
enum e_SPI_Package {
	CS_OFF,
	CS_ADC,
	CS_DAC,
	DATA_OUT,
};

enum e_SPI_LTC2600_Cmd {
WRITE_N=0b000<<4,
UPDATE_N=0b0001<<4,
WRITE_N_UPDATE_ALL=0b0010<<4,
WRITE_N_UPDATE_N=0b0011<<4,
POWER_DOWN=0b0100<<4,
NOOP=0b1111<<4
};

enum e_SPI_Device {FREE,DA_LT2600=1,AD_LTC1867=2};


static struct ringbuffer_spi {
	/* Queue Header */
    uint16_t        size;   /* maximum number of elements           */
    uint16_t       start;  /* index of oldest element              */
    uint16_t         end;    /* index at which to write new element  */
    /* Queue Body */
    uint8_t    command[SPI_RINGBUFFER_SIZE];  /* vector of commands */
    uint8_t    data[SPI_RINGBUFFER_SIZE];  /* vector of elements                   */
} __attribute__((__unused__))_spi_queue;
