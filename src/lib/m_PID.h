struct sPID{
	double P;
	double I;
	double D;
	double lasterror;
	double integrator;
};

struct sPIDf{
	float P;
	float I;
	float D;
	float lasterror;
	float integrator;
};

double PID_calc (struct sPID* argPID, double sp, double pv);
float PIDf_calc (struct sPIDf* argPID, float sp, float pv);
