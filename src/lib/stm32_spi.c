 /*  -------------------------------------------------------------
 *          SPI DAC LTC2600 Driver
 *  -------------------------------------------------------------
 *
 *
 *	    
 * -------------------------------------------------------------
 *          Pin Configuration
 *  -------------------------------------------------------------
 *
 *    SPI1 - (A.2-A.7)
 * -------------------------------------------------------------
 *          Comments Section
 *  -------------------------------------------------------------
 */


#include "includes.h"

/* Private Defines */



#define PIN_CS_DAC GPIO_Pin_8
#define PIN_CS_ADC GPIO_Pin_9
#define PORT_CS GPIOA


/*Public Prototypes */
void stm32_spi_init();
void spi_dac_set(uint8_t argChannel, uint16_t argValue);
void spi_adc_sampleAll();

/* Private Prototypes */

inline void _spi_adc_sample(uint8_t par_adc_channel);
inline void _spi_adc_empty();
inline void _spi_queue_add(uint8_t parCommand, uint8_t parData);
inline void _spi_queue_sendNext();

inline void _spi_chipSelect(u8 argDevice);
inline void _spi_chipDeselect();
inline void _spi_interrupt_txe_enable();
inline void _spi_interrupt_txe_disable();


/* Private Variables Declaration */





/*--------------------------------------------------------------------------------------
*             DAC LT2600 Driver
*--------------------------------------------------------------------------------------*/

/* Public High Level Function for Outside */
void spi_dac_set(uint8_t argChannel, uint16_t argValue){

/* Byte 0 - 4 Bit Command / 4 Bit Address */
/* Byte 1/2 - 16 Bit Value */
_spi_queue_add(CS_DAC,0);
_spi_queue_add(DATA_OUT, (u8) (WRITE_N_UPDATE_N | argChannel));
_spi_queue_add(DATA_OUT, (u8) (argValue>>8) & 0xFF);
_spi_queue_add(DATA_OUT, (u8) argValue & 0xFF);
_spi_queue_add(CS_OFF,255);
}


/*--------------------------------------------------------------------------------------
*             ADC LTC1867 Driver
*--------------------------------------------------------------------------------------*/



	void spi_adc_sampleAll(){
	
	int i;
	//_spi_adc_empty();

	for(i=0; i<SPI_ADC_CHANNELS;i++){
		_spi_adc_sample(i);
	}

	_spi_adc_empty();
	
	}

uint8_t _spi_adc_lastChannelRequested=0;
uint8_t _spi_bytes_rx=0;
//uint16_t spi_adc_values[SPI_ADC_CHANNELS];

	inline void _spi_adc_sample(uint8_t par_adc_channel){
		u8 tmpAddress;
		 switch(par_adc_channel){

		 	 	 	 /* CHx-GND Case replaced with CHx-COM
		            case 0: tmpAddress=0b10000;break;
		            case 1: tmpAddress=0b11000;break; */

		 	 	 	 case 0: tmpAddress=0b10001;break;
		 		     case 1: tmpAddress=0b11001;break;


		            case 2: tmpAddress=0b10010;break;
		            case 3: tmpAddress=0b11010;break;
		            case 4: tmpAddress=0b10100;break;
		            case 5: tmpAddress=0b11100;break;
		            case 6: tmpAddress=0b10110;break;
		            case 7: tmpAddress=0b11110;break;
		            default:
		            	 tmpAddress=0;
		            	break;
		            }
					_spi_queue_add(CS_ADC,par_adc_channel); /* This will trigger the transfer of the previously requested ADC Data as well */
					_spi_queue_add(DATA_OUT, 0b10000100 | (tmpAddress<<3)); /* 0b1xxx010 corresponds to the 3 bit channel address */
					_spi_queue_add(DATA_OUT, 0b0000000); /* Send a DUMMY Package to receive the 9-15 Bit from ADC */
		            _spi_queue_add(CS_OFF,par_adc_channel); /* This will trigger the conversion. */

		return;
	}

	inline void _spi_adc_empty(){


					u8 tmpAddress=0b10000;
					_spi_queue_add(CS_ADC,255); /* This will trigger the transfer of the previously requested ADC Data as well */
					_spi_queue_add(DATA_OUT, 0b00000100 |(tmpAddress<<3)); /* 0b1xxx010 corresponds to the 3 bit channel address */
					_spi_queue_add(DATA_OUT, 0b0000000); /* Send a DUMMY Package to receive the 9-15 Bit from ADC */
		            _spi_queue_add(CS_OFF,255); /* This will trigger the conversion. */

		return;
	}

   


/*--------------------------------------------------------------------------------------
*             SPI Tx Ringbuffer
*--------------------------------------------------------------------------------------*/

static struct ringbuffer_spi  _spi_queue;

inline uint8_t _spi_queue_waitingElements(){return (_spi_queue.end-_spi_queue.start);}
inline uint8_t _spi_queue_currentElement(){return (_spi_queue.data[_spi_queue.start]);}


/* Public Function to fill the Ringbuffer */

inline void _spi_queue_add(uint8_t parCommand, uint8_t parData){

/* Add a char to the Ring buffer */
_spi_queue.data[_spi_queue.end]=parData;
_spi_queue.command[_spi_queue.end]=parCommand;
_spi_queue.end++;

if (_spi_queue.end==_spi_queue.size)_spi_queue.end=0;

if (_spi_queue.start==_spi_queue.end){
	/* Ringbuffer Overflow! */
	printf("\n\r #Fault.SPI_Ringbuffer Overflow! RB_Size %i, POS %i",_spi_queue.size, _spi_queue.end);
	main_error(e_spi);
	return;
}


/*  Check the Transmit data register empty flag. Start New Transfer. Otherwise do nothing */
if (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == SET) {_spi_queue_sendNext();}
return;
}


/* Private Functions to Proceed to the next Byte */
inline void _spi_queue_sendNext(){
/* Add a char from Ring Buffer and add it to be send */
	if(_spi_queue.start!=_spi_queue.end){
		/*There is data to be transmitted */

		switch(_spi_queue.command[_spi_queue.start]){
		case CS_OFF: 

			_spi_bytes_rx=2;

			_spi_adc_lastChannelRequested=_spi_queue.data[_spi_queue.start];

			while( (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET ));

			_spi_chipDeselect();
			/*This starts the conversion . Now it would be necessary to wait untilt he conversion is finished.
			* The Minimum Time to Wait is 100ns. At 72 MHz one tick takes roughly 14ns.
			* Datasheet recommends to keep digital signals on one level for better conversion. After this function is finished, an TXE occurs and will run the next command.  (ChipSelect DAC/ADC.) 
			To avoid that DAC command runs instantly during ADC Conversion
			*/
            int iwait;
            for(iwait=0;iwait<10;iwait++){
            	asm("nop");
            }

		
		break;  
		case CS_ADC: 

			_spi_chipSelect(AD_LTC1867);

		break; // This will trigger receiving of last requested value
		case CS_DAC: _spi_chipSelect(DA_LT2600);break;
		case DATA_OUT :  SPI_I2S_SendData(SPI1, _spi_queue.data[_spi_queue.start]);
		
		}


			/* Calculate Position of Next Element in Ring Buffer */
             _spi_queue.start++;
			/* If one buffer round is finished */
			if (_spi_queue.start==_spi_queue.size)_spi_queue.start=0;

			_spi_interrupt_txe_enable();
	}else	{
			// There is nothing to send.
		    _spi_interrupt_txe_disable();


	}

return;
}






/*----------------------------------------------------------------------------------------------------------------*/
/* Hardware Definition--------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------------------------*/

void stm32_spi_init(void)
{

GPIO_InitTypeDef GPIO_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
SPI_InitTypeDef SPI_InitStructure;
 
 
RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO | RCC_APB2Periph_SPI1, ENABLE);
 
 	  /* Configure SPI_MASTER pins: SCK and MOSI ---------------------------------*/
	  /* Configure SCK and MOSI pins as Alternate Function Push Pull */
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_7;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
	 /* Configure SPI_MASTER pins: MISO ---------------------------------*/
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
 


	/* Reserve Pins for the Chip Select */
		GPIO_InitStructure.GPIO_Pin = PIN_CS_ADC|PIN_CS_DAC;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(PORT_CS, &GPIO_InitStructure);
 

 
SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
SPI_InitStructure.SPI_CRCPolynomial = 0;
SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
SPI_Init(SPI1, &SPI_InitStructure);


/* Init the Ringbuffer */
_spi_queue.size=SPI_RINGBUFFER_SIZE-1;
_spi_queue.start=0;
_spi_queue.end=0;

while(SPI_I2S_ReceiveData(SPI1));

SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_RXNE, ENABLE);
SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_TXE, DISABLE);
 SPI_Cmd(SPI1, ENABLE);

 NVIC_InitStructure.NVIC_IRQChannel = SPI1_IRQn;
 NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
 NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
 NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 NVIC_Init(&NVIC_InitStructure);


}





inline void _spi_chipSelect(u8 argDevice){

if (argDevice==DA_LT2600){ GPIO_WriteBit(PORT_CS, PIN_CS_DAC, RESET);
}else if (argDevice==AD_LTC1867)
{ GPIO_WriteBit(PORT_CS, PIN_CS_ADC, RESET);
}else{
	printf("#Debug Fault. Unknown SPI Chip Selected");main_error(e_spi);
}
}

inline void _spi_chipDeselect(){
GPIO_WriteBit(PORT_CS, PIN_CS_DAC, SET);
GPIO_WriteBit(PORT_CS, PIN_CS_ADC, SET);
}

inline void _spi_interrupt_txe_enable(){
SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_TXE, ENABLE);
}

inline void _spi_interrupt_txe_disable(){
SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_TXE, DISABLE);
}





/*------------------ Interrupt definitions for the SPI1-------------- */
void SPI1_IRQHandler(void){


    if(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)==SET ){
     /* Case RX Transmission Completed. */
	 
		if(_spi_bytes_rx==2)
		 {

			if(_spi_adc_lastChannelRequested<SPI_ADC_CHANNELS)
			spi_adc_values[_spi_adc_lastChannelRequested]=( (0xFF & SPI_I2S_ReceiveData(SPI1)))<<8;

			 _spi_bytes_rx--;
			 } else if (_spi_bytes_rx==1){

				 if(_spi_adc_lastChannelRequested<SPI_ADC_CHANNELS)
			 spi_adc_values[_spi_adc_lastChannelRequested]|=(0xFF & SPI_I2S_ReceiveData(SPI1));

			 _spi_bytes_rx--;
			 
			 } else {
			 // We did not expect an receive. Possibly its empty package.
				 SPI_I2S_ReceiveData(SPI1);
		 }
		
		

	} else if(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE)==SET ){
     /* Case TX Transmission Completed. Try send next one. */
		_spi_queue_sendNext();



	}

}













