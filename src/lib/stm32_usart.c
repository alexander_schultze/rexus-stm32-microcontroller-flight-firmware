 /*  -------------------------------------------------------------
 *          USART Settings
 *  -------------------------------------------------------------
 *
 *
 *	    Baud: 38k4, 8 Bits, 1 Stopbit, No Parity
 * -------------------------------------------------------------
 *          Pin Configuration
 *  -------------------------------------------------------------
 *
 *    Olimex P103- Port A - 2,3 @ USART 2
 *    Rexus Moxa- Port B - 10,11 @ USART 3
 *  -------------------------------------------------------------
 *          Communication Protocol Description
 *  -------------------------------------------------------------
 *  Layer 2 - RXBX Interface Protocol 30 Bytes Packages (SYNC2 MSG_ID1 MSG_CNT1 DATA24 CRC2)
 *  Layer 2 - RXBX Interface Protocol 22 Bytes Packages (SYNC2 MSG_ID1 MSG_CNT1 DATA16 CRC2)
 *  		   SYNC DELIMITERS:  0x16 

 * -------------------------------------------------------------
 *          Comments Section
 *  -------------------------------------------------------------
 * Further Reading on the Interrupts Stuff is in Programming Reference . Page 722 on the iNterrupts stuff */
#include "includes.h"

#define USARTx USART3



#define USARTx USART3



/* Private Variables */



static struct ringbuffer_out {
    uint16_t        size;   /* maximum number of elements           */
    uint16_t       start;  /* index of oldest element              */
    uint16_t         end;    /* index at which to write new element  */
    uint8_t    data[RINGBUFFER_SIZE];  /* vector of elements                   */
} stm32_usart_bufferTX;


uint8_t stm32_usart_bufferRX[20];

uint8_t rx_msg_count; /* Counts the outgoing RX-Packages. */
uint8_t tx_msg_count; /* Counts the outgoing RX-Packages. */

/* Private functions */
static inline void _stm32_usart_queue_sendNext()  __attribute__((always_inline));
static inline void _stm32_usart_queue_add(uint8_t data) __attribute__((always_inline));

/* ------- PROTOCOL ----------------------------------------------------------- */

/* Public Function to fill the Ringbuffer */

void _stm32_usart_protocol_sendByteArray(char *text, uint8_t length){
int i;
	for(i=0; i<length; i++){
		_stm32_usart_queue_add(text[i]);
	}
	return;
}

static inline void _stm32_usart_queue_add(uint8_t data){
/* Add a char to the Ring buffer */
stm32_usart_bufferTX.data[stm32_usart_bufferTX.start]=data;
stm32_usart_bufferTX.start++;

if (stm32_usart_bufferTX.start==stm32_usart_bufferTX.size)stm32_usart_bufferTX.start=0;

if (stm32_usart_bufferTX.start==stm32_usart_bufferTX.end){
	/* Ringbuffer Overflow! */
	main_error(e_usart);

//	printdebug("\n\r USART3 Ringbuffer Overflow!");

	return;
}

/*  Check the Transmit data register empty flag. Start New Transfer. Otherwise do nothing */
if(USART_GetFlagStatus(USARTx, USART_FLAG_TXE)==SET){
_stm32_usart_queue_sendNext();
}

/* Start the transfer */
USART_ITConfig(USARTx, USART_IT_TC, ENABLE);

return;
}

/* ------- HARDWARE ----------------------------------------------------------- */

/* Private Functions to Proceed to the next Byte */
static inline void _stm32_usart_queue_sendNext()  {
/* Add a char from Ring Buffer and add it to be send */
	if(stm32_usart_bufferTX.start!=stm32_usart_bufferTX.end){
		/*There is data to be transmitted */
			USART_SendData(USARTx,stm32_usart_bufferTX.data[stm32_usart_bufferTX.end]);
			/* Calculate Position of Next Element in Ring Buffer */
			stm32_usart_bufferTX.end++;
			/* If one buffer round is finished */
			if (stm32_usart_bufferTX.end==stm32_usart_bufferTX.size){stm32_usart_bufferTX.end=0;}
	} else {
		/* There is NO Data to be transmitted */
		USART_ITConfig(USARTx, USART_IT_TC, DISABLE);
	}
}





/*------------------ Interrupt definitions for the RS232 Port, USART3-------------- */
void USART3_IRQHandler(void){

    /* Case RXNE Byte Received */

    if(USART_GetFlagStatus(USARTx, USART_FLAG_RXNE)==SET){
	USART_ClearITPendingBit(USARTx, USART_IT_RXNE);
   /* For now, some playful functions. An ECHO */
	uint8_t received;
	received=USART_ReceiveData(USARTx);
	 /* REXUS Implementation: Wait for SYNC Signal. Then start write 24 Bytes. Then check CRC and stuff.
	 * */
	static u8 bytes_read = 0;
	static u8 read_state =0;
	/* This implements a simple state mashine. It can take the following states
	 * 0 - No signal has been received so far.
	 * 1 - One SYNC frame has been received. If a second occurs we will start collecting frames.
	 * 2 - Collecting Frames now! No other data recognition will be performed.
	 * 3 - Collecting Frames but received an anxious SYNC. If another SYNC arrives falling back to 2. By this only DOUBLE SYNC start data collection.
	 */


if (received==0x16){
	switch (read_state){
	case 0:read_state=1;return;
	case 1:read_state=2;bytes_read=0;return;
	case 2:read_state=3;break;
	case 3:bytes_read=0;read_state=2;return;
	}
}
	if (read_state>=2){
		stm32_usart_bufferRX[bytes_read++]=received;
			if (bytes_read==20){
					read_state=0;
					bytes_read=0;
			    /* A Package of 22 Bytes including SYNC has been received. Evaluate the two CRC Bytes*/
		              rx_msg_count++;

                          u16 crc16_calc=crc16(stm32_usart_bufferRX, 18);
                          u16 crc16_received=((u16)stm32_usart_bufferRX[18])<<8 | stm32_usart_bufferRX[19];
                          if (crc16_calc==crc16_received){
                        	 /* A CRC-Valid Package has been received */
                        	  main_usart_rx_handler(stm32_usart_bufferRX);
							   /* Echo Package including ACK*/
							  stm32_usart_protocol_sendPackage24(stm32_usart_bufferRX,MSGID_ACK);

							}else{
							   /* Echo Package including NACK*/
								stm32_usart_protocol_sendPackage24(stm32_usart_bufferRX,MSGID_NACK);
							}
							
							
							
							}
		}

/* This area can be used to intercept received single bytes as well and process the given information. But we will stick to the clean implementation using the custom protocoll */

    }

	/* Case TX Transmission Completed */
	    if(USART_GetFlagStatus(USART3, USART_FLAG_TC)==SET){
		USART_ClearITPendingBit(USART3, USART_IT_TC);
	    _stm32_usart_queue_sendNext();
	    }
}

/* ---- Layer 1 END-- -------------------------------------------------------------------------------------- */






/* ---- Layer 2 START Packing Byte Packages with a MSG ID and CRC Error Check ------------------------------------------------ */





extern void  stm32_usart_protocol_sendPackage24(unsigned char *text, u8 par_ID){
	/* Header Size: 4+2CRC Byte
	   Data Size: 24 Byte
           Package Size: 32 Byte */

	/*
	_stm32_usart_queue_add(SYNC);
	_stm32_usart_queue_add(SYNC);
	_stm32_usart_queue_add(msgid_code);
	_stm32_usart_queue_add(++tx_msg_count);


	u8 i;
	u16 crc = 0xFFFF;
	crc=crc16_ccitt(crc, msgid_code);
	crc=crc16_ccitt(crc, tx_msg_count);
	for (i=0; i<24; i++){
		_stm32_usart_queue_add(text[i]);
		crc = crc16_ccitt(crc, text[i]);}


	_stm32_usart_queue_add(crc>>8);
	_stm32_usart_queue_add(crc);

	*/

	stm32_usart_protocol_sendPackage(text, par_ID, 24);


  return;
};




extern void  stm32_usart_protocol_sendPackage(unsigned char *text, u8 par_ID, u8 par_length){
	/* Header Size: 7 (5+2CRC) Byte
	   Data Size: 0..255 Byte
        Package Size: 32 Byte */
	_stm32_usart_queue_add(SYNC);
	_stm32_usart_queue_add(SYNC);
	_stm32_usart_queue_add(par_length);
	_stm32_usart_queue_add(par_ID);
	_stm32_usart_queue_add(++tx_msg_count);

	/* CRC 16 Generator */
	u8 i;
	u16 crc = 0xFFFF;
	//crc=crc16_ccitt(crc, par_length);
	crc=crc16_ccitt(crc, par_ID);
	crc=crc16_ccitt(crc, tx_msg_count);

	for (i=0; i<par_length; i++){
		_stm32_usart_queue_add(text[i]);
		crc = crc16_ccitt(crc, text[i]);}

	//return crc;
	_stm32_usart_queue_add(crc>>8);
	_stm32_usart_queue_add(crc);
  return;
};


/* ---- Layer 2 END ----------- - ------------------------------------------------------------------------------------------------ ------ */


/* --- Hardware Initialization Functions START-------------------------------------------------------------------------------------------*/

void stm32_usart_init(void)
{

 RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    /* Init the Ringbuffer */
 	stm32_usart_bufferTX.size=RINGBUFFER_SIZE-1;
    stm32_usart_bufferTX.start=0;
    stm32_usart_bufferTX.end=0;

	NVIC_InitTypeDef   NVIC_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	/*
	 * RS232 Definition
	 * Baud rate: 38.4 Kbit/s standard
	 * Format: 8 bits, 1 start and stop bit, no parity
	 *
	 * STM Connection: USART3
	 */


	  /* Configure RS232 on USART3 */

	    /* TX Pin */
	    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	    GPIO_Init(GPIOB, &GPIO_InitStructure);

	    /* RX Pin */
	    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	    GPIO_Init(GPIOB, &GPIO_InitStructure);


	  /* USART3 (TX)*/
	  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	  NVIC_Init(&NVIC_InitStructure);


	  /* USART3 */
	    USART_InitTypeDef USART_InitStructure;

	    USART_InitStructure.USART_BaudRate = 38400;
	    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	    USART_InitStructure.USART_StopBits = USART_StopBits_1;
	    USART_InitStructure.USART_Parity = USART_Parity_No;
	    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	    USART_Init(USARTx, &USART_InitStructure);

	    USART_ITConfig(USARTx, USART_IT_TC, ENABLE);
	    USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
	    USART_Cmd(USARTx, ENABLE);




}


/* --- Hardware Initialization Functions END-------------------------------------------------------------------------------------------*/

