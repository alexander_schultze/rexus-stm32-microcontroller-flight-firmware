/*  -------------- Driver for Continuos Conversion on ADC at STM32F1 -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze
 *  Date  :		22.04.2013
 *
 *
 * -------------------------------------------------------------
 *			Pins Configuration Overview
 * -------------------------------------------------------------
 *
 *
 *  PortC - 0,1,2,3,4,5,6,7,8,9,10 (Analog Input)
 * -------------------------------------------------------------
 *			Comments
 * -------------------------------------------------------------
 * This function will initialize the adc converters and set them up with a given task list
 *
 * The purpose is to synchronize ADC1&2 for precise measurement of U/I.
 */



#include "includes.h"

uint32_t stm32_adc_ADCBuffer[NUMBER_OF_PARALLEL_CHANNELS] = {0x00010001, 0x00010001, 0x00010001, 0x00010001,0x00010001, 0x00010001, 0x00010001};
/* This buffer works 0xFFFF0000 is ADC2 result  (12oo16)
 * 					 0x0000FFFF is ADC1 result  (12oo16)
 */

void stm32_adc_init(){
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;




/* This is for using the internal temperature sensor */
ADC_TempSensorVrefintCmd(ENABLE);


DMA_InitStructure.DMA_BufferSize = NUMBER_OF_PARALLEL_CHANNELS;
/* Important! Buffer size must equal the number of channels to be scanned! Otherwise inconsistency */

DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)stm32_adc_ADCBuffer;
DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
DMA_InitStructure.DMA_Priority = DMA_Priority_High;
DMA_Init(DMA1_Channel1, &DMA_InitStructure);

DMA_Cmd(DMA1_Channel1, ENABLE);

RCC_ADCCLKConfig(RCC_PCLK2_Div6);

GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1|  GPIO_Pin_2|  GPIO_Pin_3;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
GPIO_Init(GPIOA, &GPIO_InitStructure);

GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
GPIO_Init(GPIOB, &GPIO_InitStructure);


GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1|  GPIO_Pin_2|  GPIO_Pin_3|  GPIO_Pin_4|  GPIO_Pin_5;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
GPIO_Init(GPIOC, &GPIO_InitStructure);

ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
ADC_InitStructure.ADC_Mode = ADC_Mode_RegSimult;
ADC_InitStructure.ADC_NbrOfChannel = NUMBER_OF_PARALLEL_CHANNELS;
ADC_InitStructure.ADC_ScanConvMode = ENABLE;
ADC_Init(ADC1, &ADC_InitStructure);
ADC_Init(ADC2, &ADC_InitStructure);

/* Important! Buffer size must equal the number of channels to be scanned! Otherwise inconsistency */

ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 2, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 3, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 4, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 5, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 6, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC1, ADC_Channel_16, 7, ADC_SampleTime_239Cycles5);

ADC_RegularChannelConfig(ADC2, ADC_Channel_1, 1, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC2, ADC_Channel_3, 2, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC2, ADC_Channel_9, 3, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC2, ADC_Channel_11, 4, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC2, ADC_Channel_13, 5, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC2, ADC_Channel_15, 6, ADC_SampleTime_239Cycles5);
ADC_RegularChannelConfig(ADC2, ADC_Channel_17, 7, ADC_SampleTime_239Cycles5);

/* The additional channels ADC_16 is Internal Temperature, ADC_17 is Internal Voltage Reference */
ADC_ExternalTrigConvCmd(ADC1, ENABLE);
ADC_ExternalTrigConvCmd(ADC2, ENABLE);
ADC_Cmd(ADC1, ENABLE);
ADC_Cmd(ADC2, ENABLE);

ADC_DMACmd(ADC1, ENABLE);

ADC_ResetCalibration(ADC1);
while(ADC_GetResetCalibrationStatus(ADC1));
ADC_StartCalibration(ADC1);
while(ADC_GetCalibrationStatus(ADC1));

ADC_ResetCalibration(ADC2);
while(ADC_GetResetCalibrationStatus(ADC2));
ADC_StartCalibration(ADC2);
while(ADC_GetCalibrationStatus(ADC2));

ADC_SoftwareStartConvCmd(ADC1, ENABLE);

ADC_SoftwareStartConvCmd(ADC2, ENABLE);



}
