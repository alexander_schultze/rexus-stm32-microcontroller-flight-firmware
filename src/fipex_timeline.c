/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		13.07.2013
 */
#include "includes.h"

void timeline_led_soe_toggle();

void timeline_run(){

	//timeline_led_soe_toggle();


	switch(fipex_time_lo) {
	    case 0:
	    	break;
		case 01:    //Acknowledge liftoff
			{
			main_bat_on();
			printf("\r\n[MB%u]Liftoff", MAINBOARD_ID);
			}break;
		case 3650: //Prepare for impact ;-)
			{	
			main_stopAll();
			}
		default: break;


	}

}


void timeline_led_soe_toggle()
{
	if((fipex_time_lo%2) == 0) {
		main_led_soe_off();
	} else {
		main_led_soe_on();
	}

}
