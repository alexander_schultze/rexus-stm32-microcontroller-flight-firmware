﻿/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		13.07.2013
 */
#include "includes.h"

// Private Function Prototypes
void fipex_updateGain(u8 i);
void fipex_setGain(u8 i);


/**
  * @brief  Main Control Loop. According to state, PID will be invoked.
  * @signal DAC Outputs. (directly via SPI)
  * @param  ADC Inputs.	 (indirectly via Memory)
  * @retval None
  */
void fipex_control(void){
#ifdef useTaskMonitor
	fipex_scheduler_perfmon_starttask(4);
#endif


	/* Here is a good time to check for the different States and the corresponding actions

	typedef enum {
	SETUP= 1,
	IDLE,
	HEATING_ONLY,
	MEASURE,
	FLIGHT,
	STOP,
	TEST,
	TESTPINS
} EXP_STATE;
	 */


   // The ADC Values are still not put into the right order yet. We will have to sort them. Idealy, this should happen by a DMA handler. This is a workaround.
    _fipex_retrieve_ADC_values();

   u8 i;
	for(i=0; i<6; i++){

		if((current_state==HEATING_ONLY)||(current_state==MEASURE)||(current_state==FLIGHT)){

			switch(_sDevices[i].Device){
			case SENSOR1:
			case SENSOR2:
			case SENSOR3:_sDevices[i].setValue=fipex_control_sensor(i);break;
			case HEATING1:
			case HEATING2:
			case HEATING3:_sDevices[i].setValue=fipex_control_heating(i);break;
			default: break; //Error!

			}

		} else if (current_state==TEST_FEEDBACK){

			_sDevices[i].setValue=fipex_adc_values[_sDevices[i].ADC_Channel_U]<<4;
			// Shift left for 12Bit-> 16Bit
		} else{
			_sDevices[i].setValue=0;
			/* Output 0 to all  */

		}


		spi_dac_set(_sDevices[i].DAC_Channel,(uint16_t) _sDevices[i].setValue);

	}







#ifdef useTaskMonitor
	fipex_scheduler_perfmon_starttask(255);
#endif
}


inline u16 fipex_control_heating(u8 i){


	// This section is for TEST Boards
//	const double gain_U =  (3.4+1.0)/1.0;
//	const double gain_I = 1.0/(39.95*0.1);
//	const double gain_U_set = 2.0/(18.0+2.0);

	// This section is for FLIGHT Boards
	const double gain_U =  1.0/(1.5+1.0);
	const double gain_I = 1.0/(39.95*0.1);
	const double gain_U_set = 2.0/(3.0+2.0);

	_sDevices[i].ADC_U =fipex_adc_values[_sDevices[i].ADC_Channel_U];
	_sDevices[i].ADC_I =fipex_adc_values[_sDevices[i].ADC_Channel_I];


    double U_ist,I_ist, U_soll;
    /* Conversion to the correct Input Values from ADC */
	U_ist = ((double)_sDevices[i].ADC_U)*3.3/__pow(2,12);
	I_ist = ((double)_sDevices[i].ADC_I)*3.3/__pow(2,12);
	/* Conversion according to the sensor board */
	U_ist = U_ist*gain_U;
	I_ist = I_ist*gain_I;


	/* Calculate the Process Value Parameters */

   /*(R_ist )*/
   //_sDevices[i].processValue =U_ist / I_ist;
   double R_ist= U_ist / I_ist;

   /* Floating Averge Filter */
   double alpha=0.1;
   _sDevices[i].processValue=(R_ist)*(alpha)+(_sDevices[i].processValue)*(1-alpha);




    /* In case that the processValue is out of our expected bounds, there is an error with measurement. Maybe U and I is 0. Return a careful start value */
    if(_sDevices[i].processValue>20) _sDevices[i].processValue=2.5;


	 /* R_set = PID (R_target-R_process)*/

    double R_soll = PID_calc(&(_sDevices[i].PID),_sDevices[i].targetValue,_sDevices[i].processValue);

	 /* The corresponding U to be set */
	 U_soll = R_soll * I_ist;

	 /* Limit I to 0.500mA according to Resistance */
	 double U_limit = (_sDevices[i].processValue*0.5) ;
			 if(U_soll>U_limit)
				 {
				 U_soll=U_limit;
				 }

/* Bedingung zum setzen eines minimalen Startwertes der mindestens gesetzt muss */
     if(U_soll<0.1){
    	 U_soll=0.1;

    	 /* AWR: It might be constantly not connected. In this case, limit the integrator */
    //	 if(_sDevices[i].PID.integrator>2000){
    //	 _sDevices[i].PID.integrator=0;
    //	 _sDevices[i].PID.lasterror=0;}

     }


	 /* Convert this again to a 16 BIT ADC Signal, 3.3Volt */

	 u16 U_setValue;

	 U_soll = 3.2;
	 U_soll = U_soll * gain_U_set;


	if(U_soll<=0){
		U_setValue=0;
	} else if (U_soll>=3.3){

		U_setValue=0xFFFF;
	} else {
		 U_setValue = (u16) ((U_soll/3.3)*__pow(2,16));

	}

	 /* Saturation: Limit the Output to the Maximum Value */
	// if(U_setValue>0x0FFFF)U_setValue=0x0FFF;

#ifdef debugPID
	    if(i==3){
	    printf("\r\n Heating(%i): ADC1 %i mV; ADC2 %i mV;",i,(int) (( (double) _sDevices[i].ADC_U)*3300/__pow(2,12)),(int) (( (double) _sDevices[i].ADC_I)*3300/__pow(2,12)));
	    printf(" U_ist: %f; I_ist: %f;", (U_ist), (I_ist));
	    printf(" U_soll: %f (max %f);", (U_soll), U_limit);

	    printf("\r\nPV: %f; TV: %f; SV: %f",_sDevices[i].processValue,_sDevices[i].targetValue,_sDevices[i].setValue);
	    }

	    if(i==0){
	    printf("\r\n Sensor: ADC1 %i mV; ADC2 %i mV;",i,(int) (( (double) _sDevices[i].ADC_U)*3300/__pow(2,12)),(int) (( (double) _sDevices[i].ADC_I)*3300/__pow(2,12)));
	    printf(" Gain: %i;", _sDevices[i].Gain);
	    }

#endif



	 return U_setValue;

}

inline u16 fipex_control_sensor(u8 i){
	_sDevices[i].ADC_U =fipex_adc_values[_sDevices[i].ADC_Channel_U];
	_sDevices[i].ADC_I =fipex_adc_values[_sDevices[i].ADC_Channel_I];
	/* We need to evaluate the Current Measurement Range and update the Gain if Necessary */

	


	/* Update the Gain Output Pins */
	fipex_updateGain(i);

	u16 U_setValue=0;
	if((i==1)||(i==2)||(i==3)){
    /* Nothing needs to be controlled here for O2 or O3. After we set the target Voltage it will be directly output. */
	U_setValue =_sDevices[i].targetValue/3.3*__pow(2,16);
	}else if (i==0){
		/* Invoke a PID around Working Point: Reference Voltage
		 *
		 * Mapping:
		 * ADC_U CH4 ADC2$1 U_set (gesetzte Spannung)
		 * ADC_I CH5 ADC2$2 U_Ref (Mittelabgriff)
		 * */
	_sDevices[i].processValue = ((double)_sDevices[i].ADC_I)*3.3/__pow(2,12);

	double U_soll = PID_calc(&(_sDevices[i].PID),_sDevices[i].targetValue,_sDevices[i].processValue);
    if(U_soll<=0.0){U_soll=0;}
    if(U_soll>=1.8){U_soll=1.8;}
	U_setValue =(u16) (U_soll/3.3*__pow(2,16));
	}
	 /* Saturation: Limit the Output to the Maximum Value, 16bit DAC*/
     if(U_setValue>0x0FFFF)U_setValue=0xFFFF;
  	return U_setValue;
}





void fipex_control_init(){
/* -------------------Sensors---------------------------- */
	/* -------Sensor 1---------- */
				 _sDevices[0].Device=SENSOR1;
				 _sDevices[0].Port_GainSelect=GPIOC;
				 _sDevices[0].Pin_Gain1=GPIO_Pin_6;
				 _sDevices[0].Pin_Gain2=GPIO_Pin_7;
				 _sDevices[0].ADC_Channel_U=0;
				 _sDevices[0].ADC_Channel_I=1;
				 _sDevices[0].DAC_Channel=0;
				 _sDevices[0].Gain=GAIN1;
				 fipex_setGain(0);

				 _sDevices[0].PID.P=0.8;
				 _sDevices[0].PID.I=0.012;
				 _sDevices[0].PID.D=0.01;
				 _sDevices[0].PID.lasterror=0.0;
			   	 _sDevices[0].PID.integrator=0.0;

				_sDevices[0].targetValue=0.7; /* Volt */

	/* -------Sensor 2---------- */
				 _sDevices[1].Device=SENSOR2;
				 _sDevices[1].Port_GainSelect=GPIOC;
				 _sDevices[1].Pin_Gain1=GPIO_Pin_8;
				 _sDevices[1].Pin_Gain2=GPIO_Pin_9;
				 _sDevices[1].ADC_Channel_U=4;
				 _sDevices[1].ADC_Channel_I=5;
				 _sDevices[1].DAC_Channel=2;
				 _sDevices[1].Gain=GAIN1;
				 fipex_setGain(1);

				_sDevices[1].targetValue=0.7; /* Volt */

		/* -------Sensor 3---------- */
				 _sDevices[2].Device=SENSOR3;
				 _sDevices[2].Port_GainSelect=GPIOC;
				 _sDevices[2].Pin_Gain1=GPIO_Pin_10;
				 _sDevices[2].Pin_Gain2=GPIO_Pin_11;
				 _sDevices[2].ADC_Channel_U=8;
				 _sDevices[2].ADC_Channel_I=9;
				 _sDevices[2].DAC_Channel=4;
				 _sDevices[2].Gain=GAIN1;
				 fipex_setGain(2);


				_sDevices[2].targetValue=0.7; /* Volt */
/* -------------------Heatings---------------------------- */
			/* -------Heating 1---------- */
				 _sDevices[3].Device=HEATING1;
				 _sDevices[3].ADC_Channel_U=2;
				 _sDevices[3].ADC_Channel_I=3;
				 _sDevices[3].DAC_Channel=1;
				 _sDevices[3].Gain=GAIN_OFF;


				 /* Target Value

				_sDevices[3].PID.P=0.1;
				_sDevices[3].PID.I=0.1;
				_sDevices[3].PID.D=0.01; */

				 _sDevices[3].PID.P=0.01;
				 _sDevices[3].PID.I=0.005;
				 _sDevices[3].PID.D=0.01;


				_sDevices[3].PID.lasterror=0.0;
				_sDevices[3].PID.integrator=0.0;

			    _sDevices[3].processValue=fipex_PT_calcResistance(25,3.0); /* °C / Ro*/
				_sDevices[3].targetValue=fipex_PT_calcResistance(500,3.0); /* °C / Ro*/

			/* -------Heating 2---------- */
				 _sDevices[4].Device=HEATING2;

				 _sDevices[4].DAC_Channel=0;
				 _sDevices[4].ADC_Channel_U=6;
				 _sDevices[4].ADC_Channel_I=7;
				 _sDevices[4].DAC_Channel=3;
				 _sDevices[4].Gain=GAIN_OFF;

				 _sDevices[4].PID.P=0.01;
				 _sDevices[4].PID.I=0.005;
				 _sDevices[4].PID.D=0.01;
				_sDevices[4].PID.lasterror=0.0;
				_sDevices[4].PID.integrator=0.0;

			    _sDevices[4].processValue=fipex_PT_calcResistance(25,2.5); /* °C / Ro*/
				_sDevices[4].targetValue=fipex_PT_calcResistance(500,2.5); /* °C / Ro*/
			/* -------Heating 3---------- */
				 _sDevices[5].Device=HEATING3;

				 _sDevices[5].DAC_Channel=0;
				 _sDevices[5].ADC_Channel_U=10;
				 _sDevices[5].ADC_Channel_I=11;
				 _sDevices[5].DAC_Channel=5;
				 _sDevices[5].Gain=GAIN_OFF;


				 _sDevices[5].PID.P=0.0;
				 _sDevices[5].PID.I=0.0;
				 _sDevices[5].PID.D=0.0;
				_sDevices[5].PID.lasterror=0.0;
				_sDevices[5].PID.integrator=0.0;

			    _sDevices[5].processValue=fipex_PT_calcResistance(25,2.5); /* °C / Ro*/
				_sDevices[5].targetValue=0; /* °C / Ro*/


double cable_resistance=0.15; /* Ohms  */
/* Device Specific Values here */
#if (MAINBOARD_ID==15)
/* 1: AO */
_sDevices[0].targetValue=0.5; /* Volt */
_sDevices[3].targetValue=(3.6*2.65)+cable_resistance;  /* Ohms  */
/* 2: O2 */
_sDevices[1].targetValue=0.8; /* Volt */
_sDevices[4].targetValue=(2.86*3.15)+cable_resistance;  /* Ohms  */
/* 3: O3 */
_sDevices[2].targetValue=0.75; /* Volt */
#endif

#if (MAINBOARD_ID==16)||(MAINBOARD_ID==12)
/* 1: AO */
_sDevices[0].targetValue=0.5; /* Volt */
_sDevices[3].targetValue=(3.7*2.65)+cable_resistance;  /* Ohms  */
/* 2: O2 */
_sDevices[1].targetValue=0.8; /* Volt */
_sDevices[4].targetValue=(2.9*3.2)+cable_resistance;  /* Ohms  */
/* 3: O3 */
_sDevices[2].targetValue=0.75; /* Volt */
#endif



/* ------------------PIN Initializations ----------------------- */
GPIO_InitTypeDef   GPIO_InitStructure;
   int i;
	for(i=0; i<3; i++){


	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	 GPIO_InitStructure.GPIO_Pin = _sDevices[i].Pin_Gain1|_sDevices[i].Pin_Gain2;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_Init(_sDevices[i].Port_GainSelect, &GPIO_InitStructure);

	}

}

inline void fipex_updateGain(u8 i){
/* Grenze für die Wertumschaltung 10%.
 * Switch Up: 90%> (
 * Switch Down: 10%<
 */

/*  Min Value -> Target Value (which is @ I=0) */
/*  Max Value -> 3.3V */


	/*setValue is 16 Bit. ADC_U is 12 Bit */
	s32 tmpUdiff=_sDevices[i].ADC_U - (_sDevices[i].setValue>>4);

    /* 0x0080 ->  0,103 Volt */

	if (tmpUdiff<0x0080){
		if(_sDevices[i].Gain<4){
		/* The gain can still increase */
			++_sDevices[i].Gain;
		}


	} // If the Adc input is close to saturation, gain down //
	else if(_sDevices[i].ADC_U>=0x0F80){
		if(_sDevices[i].Gain>1){
		/* The gain can still decrease */
			--_sDevices[i].Gain;
		}
		if(i==1){

			_sDevices[i].Gain;
		}


	}
	/* Update the Gain Output Pins */
	fipex_setGain(i);
}

inline void fipex_setGain(u8 i){

	u8 tmpA1, tmpA2;

	switch(_sDevices[i].Gain){
	case GAIN_OFF:{return;}break;
	case GAIN1: {tmpA1=Bit_SET;tmpA2=Bit_SET;}break;
	case GAIN10: {tmpA1=Bit_SET;tmpA2=Bit_RESET;}break;
	case GAIN100: {tmpA1=Bit_RESET;tmpA2=Bit_SET;}break;
	case GAIN1000: {tmpA1=Bit_RESET;tmpA2=Bit_RESET;}break;
	default: return;
	}
	/* Output to the PIns */
	GPIO_WriteBit(_sDevices[i].Port_GainSelect, _sDevices[i].Pin_Gain1, tmpA1);
	GPIO_WriteBit(_sDevices[i].Port_GainSelect, _sDevices[i].Pin_Gain2, tmpA2);
}




/*
 PID = GainP * actual error + GainI * SUM(previous errors) + GainD * (actual error - last error)

error = sp(set point) - pv(process value)

P_err = err;
I_err += err_old;
D_err = err - err_old;

return 0.1*P_err + 0.3*I_err + 0.02*D_err;*/

double fipex_PT_calcResistance(u16 parTemp, double  parRo){
	double  a=3.9038e-3;
	double  b=-3.5755e-7;
	double  result;
	result = parRo*(1+a*parTemp+b*b*parTemp);
	return result;
}


