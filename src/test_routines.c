#include "stm32f10x.h"
#include "includes.h"

double pow(double,double);



void test_wait(uint32_t parDelay){

	while(parDelay--)asm("");



}

void test_output(){
while(1){
	printf("**");
    test_wait(1);
}


}

void test_dac(){

	stm32_spi_init();
	/* Sägezahnsignal an den DAC Konverter */
	uint16_t dacOutput=0;
	while(1){
	dacOutput+=200;
	spi_dac_set(0xF,dacOutput);


	//dacOutput+=2;

	test_wait(10000);

}

}

extern void fipex_setGain(u8 i);

void test_sensor_gains(){
	fipex_control_init();
	uint8_t i=0,j=0;
	_sDevices[i].Gain=0;
	for (i=0; i<3; i++){

	for (j=0; j<4; j++){
   _sDevices[i].Gain++;
   fipex_setGain(i);
	}

	}


}

void test_adc(){

	stm32_adc_init();

	while(1){
	int i;
	printf("\r\n");
	for(i=0; i< 14; i++){
	_fipex_retrieve_ADC_values();
    printf("(%i) %i;",i,fipex_adc_values[i]);
	}
	}
}

void test_spi_adc(){

	stm32_spi_init();

    while(1){
    spi_adc_sampleAll();
    printf("\r\n");
    int i;
    for (i=0; i<5; i++){


	double tmp = (((double)spi_adc_values[i])*4096)/pow(2,16);
	printf("(%i) %i;",i,(int) tmp );

    }
	test_wait(10000000);
    }
}

void test_control(){
	fipex_init();
	stm32_spi_init();
	stm32_adc_init();
	fipex_control_init();
	//current_state=HEATING_ONLY;
	current_state=HEATING_ONLY;
    u16 setValue =(0.5/3.3)*__pow(2,16);
	spi_dac_set(_sDevices[3].DAC_Channel,setValue);

	while(1){

		fipex_control();
		test_wait(10000000);





	}



}






