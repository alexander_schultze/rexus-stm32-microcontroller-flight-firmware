
/******************************************************************************/
/*            STM32F10x Peripherals Interrupt Handlers                        */
/******************************************************************************/


/**
  * @brief  This function handles Tim4 (1second) interrupt request.
  * @signal Liftoff
  * @param  None
  * @retval None
  */

void TIM4_IRQHandler();



/******************************************************************************/
/*            STM32F10x Fault Handlers                                        */
/******************************************************************************/

void MemManage_Handler(void);


void HardFault_Handler(void);


void BusFault_Handler(void);


void UsageFault_Handler();

void DebugMon_Handler();

void DMA1_Channel6_IRQHandler(void);

void DMA1_Channel8_IRQHandler(void);
