// Various Code Hacks for Testing.

/*------------------------------------------------------------------------------------------*/

#include "includes.h"



void test_i2c(){
current_state=SETUP;


	// Initializes the I2C peripherals and used GPIO pins

I2C1_LowLevel_Init();

	while(1)
	{
		 ;
    printf("PT1000: %i C\n",I2C_read_PT1000());
	printf("LM75: %i C\n",I2C_read_LM75( 0b000));
    }



}
/* Testing of Control Structures */

float PT1(float u, float K, float T){
static float yn=0; /*vorheriger Wert speichern */

float result = T*(K*u-yn)+yn;
	yn=result;
return result;
}




void test_pid(){

struct sPID argPID;
argPID.P=10;
argPID.I=0.1;
argPID.D=0.01;
argPID.lasterror=0.0;
argPID.integrator=0.0;
float sp=0;

float u=0;
float y=0;

int i;
printf("\r\n soll;ist;stell;error;");
printf("\r\n w(t);y(t);u(t);e(t);");




for(i=0; i<50;i++){

	if(i==1) sp=100;
	if(i==30) sp=50;




	u = PID_calc(&argPID,sp,y);

	/* Wertebereich Clipping. Damit modelliere ich jetzt das Werteausgang beschränkt ist. */
	if(u>150)u=150;
	if(u<0)u=0;

	y = PT1(u,1,0.1);


	printf("\r\n %i;%i;%i;%i",(int)sp, (int)y,(int)u,(int)sp-(int)y);

}
while(1){};

}



