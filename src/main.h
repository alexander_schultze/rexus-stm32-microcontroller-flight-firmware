﻿#include "stm32f10x.h"
#include "ff.h"


/* Public variables -----------------------------------------------*/

volatile short rexus_liftoff;
volatile short rexus_soe;
uint32_t program_runs;




uint8_t scheduler_10ms_request_timeout;
uint8_t scheduler_100ms_request_timeout;

/* It doesnt know the Files where to declare globally so have to include the ff.h here */
#ifdef useFatFS
FATFS fs[1];
FIL file_log, file_measurements;
#endif


typedef enum {
	SETUP= 0,
	IDLE=2,
	HEATING_ONLY,
	MEASURE,
	FLIGHT,
	STOP,
	TEST,
	TEST_FEEDBACK
} EXP_STATE;
volatile EXP_STATE current_state;



typedef enum {
	e_sdcard	= 1,
	e_spi		= 1<<1,
	e_adc		= 1<<2,
	e_timeout	= 1<<3,
	e_i2c		= 1<<4,
	e_usart		= 1<<5

} eErrorFlags;

typedef enum {
	s_i_lo			= 1,
	s_i_soe			= 1<<1,
	s_o_batteryOn	= 1<<2,

} eStatusFlags;

typedef enum {
	h_i_in1			= 1,
	h_i_in2			= 1<<1,
	h_i_doneCharge	= 1<<2,
	h_o_chargeOn	= 1<<3,
	h_o_hatchOn		= 1<<4,
} eHatchFlags;





/* Definition of the various Debug Levels. The responding implementation can be found in printf()
 * Be careful since Semihosting only works with JTAG Attached. If not - program will halt !!
 *
 * 0 - No Debug Output
 * 1 - USART
 * 2   SD Card, USART
 * 3 - SD Card, USART, LCD
 * 4 - JTAG Debug Output (Semihosting,SD Card, USART)
 * 5 - JTAG Debug Output (Semihosting,SD Card, USART, LCD)
 */
	#define EVALBOARD

	#define FALSE 0
	#define TRUE 1

/* Sauber Variante w�re die PIns �ber Defines - sorgt aber auch nicht f�r �bersichtlichkeit da
 *  wir den Code ja nicht mehrfach verwenden oder weitergeben. Ich schlage vor immer direkt im Code anzupassen

   //#define REXUSBOARD
	#ifdef EVALBOARD
	#define PORT_LED GPIOC
	#define PIN_LED1 GPIO_Pin_12
	#define PIN_LED2 NC
	#define PIN_LED3 NC
	#define PIN_LED4 NC

	#endif  */


/*----------- Function Prototypes -------------------------- */
void main_usart_rx_handler(uint8_t *data);
void inline main_led_status_on();
void inline main_led_status_off();


void inline main_led_soe_on();
void inline main_led_soe_off();

void inline main_hatch_on();
void inline main_hatch_off();

void inline main_hatch_charge_on();
void inline main_hatch_charge_off();

void inline main_bat_on();
void inline main_bat_off();

u8 inline main_hatch_in1();
u8 inline main_hatch_in2();
u8 inline main_hatch_charge_done();

void inline main_hatch_trigger_on();

u8 inline main_lo_in();
u8 inline main_soe_in();

void inline main_getHatchFlags();
void inline main_getStatusFlags();


void main_error(u8);
void main_stopAll();








