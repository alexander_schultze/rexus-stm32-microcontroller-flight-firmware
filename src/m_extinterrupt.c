/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		10.10.2013
 *
 */

#include "includes.h"

inline void exti_init()
{
	NVIC_InitTypeDef   NVIC_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	EXTI_InitTypeDef   EXTI_InitStructure;

  /* Enable GPIOA clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

  /* Configure PB.05 pin as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Enable AFIO clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

  /* Connect EXTI0 Line to PB.05 pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource5);

  /* Configure EXTI5 line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line5;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI0 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  return;
}


/**
  * @brief  This function handles External line 0 interrupt request.
  * @signal Liftoff
  * @param  None
  * @retval None
  */
void EXTI9_5_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line5) != RESET)
  {



/* Liftoff */

		 if ((current_state==IDLE)||(current_state==HEATING_ONLY)||(current_state==FLIGHT))
		 {
			 main_led_soe_on();
			/* Clear the  EXTI line 0 pending bit */
			EXTI_ClearITPendingBit(EXTI_Line5);

			uint16_t countdown=65000;
		    while(countdown--)
		    {
		    	asm("");
		    }

		    if(!main_lo_in()){

		    	current_state=FLIGHT;
		    	}
		 }
  }
  return;
}
