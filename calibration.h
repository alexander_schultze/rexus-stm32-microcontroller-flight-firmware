#include "setup.h"
/*  -------------- FIPEX Measurement on REXUS -----------------
 *
 *  Rexus 15/16 Team MOXA Implementation
 *  Target: 	STM32F103RB
 *  Author: 	A. Schultze 2013
 *  Date  :		15.02.2013
 *
 *
 * -------------------------------------------------------------
 *			Sensor Calibration
 * -------------------------------------------------------------
 */


#if MAINBOARD_ID ==10

#define CALIB_PRESSURE1 == (double)0.4
#define CALIB_PRESSURE2 == (double)0.4

#endif
